package org.opisms.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.blankj.utilcode.util.Utils;


@SuppressWarnings("ALL")
public class AppController extends Application {
	
	
	public static final String TAG = Application.class.getSimpleName();
	private static AppController instance;
	private static Context context;
	TelephonyManager telephonyManager;
	public static String stringDeviceId;
	
	
	@Override
	public void onCreate() {
		super.onCreate();
		try {
			context = getApplicationContext();
			Utils.init(this);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	
	/**
	 *
	 * @return
	 */
	public static Context getContext() {
		return context;
	}
	
	
	/**
	 *
	 * @param context
	 * @param newTopActivityClass
	 */
	public static void startNewActivity(Context context, Class<? extends Activity> newTopActivityClass) {
		Intent intent = new Intent(context, newTopActivityClass);
		context.startActivity(intent);
	}
	
	
	/**
	 *
	 * @param context
	 * @param newTopActivityClass
	 */
	public static void startNewTopActivity(Context context, Class<? extends Activity> newTopActivityClass) {
		Intent intent = new Intent(context, newTopActivityClass);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK which is only available from API level 11
		}
		context.startActivity(intent);
	}
	
	
	/**
	 *
	 * @param context
	 * @param intent
	 */
	public static void startNewTopActivityFromIntent(Context context, Intent intent) {
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK which is only available from API level 11
		}
		context.startActivity(intent);
	}
	
	
	public static synchronized AppController getInstance() {
		return instance;
	}
	
}
