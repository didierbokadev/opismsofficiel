package org.opisms.app.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.UriUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.opisms.app.adapter.FamilyAdapter.OnFamilyAdapterListener;
import org.opisms.app.fragment.ForgetPasswordFragment;
import org.opisms.app.fragment.ForgetPasswordFragment.OnForgetPasswordFragmentListener;
import org.opisms.app.fragment.FullscreenImageFragment;
import org.opisms.app.fragment.FullscreenImageFragment.OnFullscreenImageFragmentListener;
import org.opisms.app.fragment.RegisterFragment;
import org.opisms.app.fragment.RegisterFragment.OnRegisterFragmentListener;
import org.json.JSONObject;
import org.opisms.app.R;
import org.opisms.app.adapter.SmsUserAdapter.OnSmsUserAdapterListener;
import org.opisms.app.adapter.Vaccine2Adapter.OnVaccine2AdapterListener;
import org.opisms.app.adapter.VaccineAdapter.OnVaccineAdapterListener;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.fragment.AboutFragment;
import org.opisms.app.fragment.AboutFragment.OnAboutFragmentListener;
import org.opisms.app.fragment.CarnetDashbordFragment;
import org.opisms.app.fragment.CarnetDashbordFragment.OnCarnetDashbordFragmentListener;
import org.opisms.app.fragment.DashboardFragment;
import org.opisms.app.fragment.DashboardFragment.OnDashboardFragmentListener;
import org.opisms.app.fragment.DetailsSmsUserFragment;
import org.opisms.app.fragment.DetailsSmsUserFragment.OnDetailsSmsUserFragmentListener;
import org.opisms.app.fragment.FamiliesListFragment;
import org.opisms.app.fragment.FamiliesListFragment.OnFamiliesListFragmentListener;
import org.opisms.app.fragment.LoginFragment;
import org.opisms.app.fragment.LoginFragment.OnLoginFragmentListener;
import org.opisms.app.fragment.ProfilFragment;
import org.opisms.app.fragment.ProfilFragment.OnProfilFragmentListener;
import org.opisms.app.fragment.ReabonnementFragment;
import org.opisms.app.fragment.ReabonnementFragment.OnReabonnementFragmentListener;
import org.opisms.app.fragment.SmsUserFragment;
import org.opisms.app.fragment.SmsUserFragment.OnSmsUserFragmentListener;
import org.opisms.app.fragment.UpdatePasswordFragment;
import org.opisms.app.fragment.UpdatePasswordFragment.OnUpdatePasswordFragmentListener;
import org.opisms.app.fragment.VaccinesIncomeFragment;
import org.opisms.app.fragment.VaccinesIncomeFragment.OnVaccinesIncomeFragmentListener;
import org.opisms.app.fragment.VaccinesListFragment.OnVaccinesListFragmentListener;
import org.opisms.app.fragment.VaccinesMissedFragment;
import org.opisms.app.fragment.VaccinesMissedFragment.OnVaccinesMissedFragmentListener;
import org.opisms.app.fragment.VaccinesRightFragment;
import org.opisms.app.fragment.VaccinesRightFragment.OnVaccinesRightFragmentListener;
import org.opisms.app.fragment.WriteUsFragment;
import org.opisms.app.fragment.WriteUsFragment.OnWriteUsFragmentListener;
import org.opisms.app.model.CalendrierVaccinal;
import org.opisms.app.model.CommonResponse;
import org.opisms.app.model.Family;
import org.opisms.app.model.Patient;
import org.opisms.app.model.SmsUser;
import org.opisms.app.model.Vaccine;
import org.opisms.app.network.OPISMSApiClient;
import org.opisms.app.network.OPISMSApiServices;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.ImageFilePath;
import org.opisms.app.utils.UrlBanks;
import org.opisms.app.worker.CalendrierWorker;
import org.opisms.app.worker.FamilyWorker;
import org.opisms.app.worker.SmsWorker;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@SuppressWarnings("ALL")
public class ContainerActivity extends AppCompatActivity implements OnLoginFragmentListener, OnDashboardFragmentListener,
        OnProfilFragmentListener, OnSmsUserFragmentListener, OnSmsUserAdapterListener, OnDetailsSmsUserFragmentListener,
        OnVaccinesListFragmentListener, OnVaccineAdapterListener, OnFamiliesListFragmentListener, OnAboutFragmentListener,
        OnCarnetDashbordFragmentListener, OnVaccine2AdapterListener, OnVaccinesRightFragmentListener, OnVaccinesMissedFragmentListener,
        OnReabonnementFragmentListener, OnVaccinesIncomeFragmentListener, OnUpdatePasswordFragmentListener, OnWriteUsFragmentListener,
        OnRegisterFragmentListener, OnFamilyAdapterListener, OnForgetPasswordFragmentListener, OnFullscreenImageFragmentListener {


    AppCompatImageView ivAppBarAction;
    AppCompatImageView ivAppBarMenu;
    AppCompatTextView tvAppBarTitle;
    FrameLayout flWrapper;
    RelativeLayout rlAppBarContainer;
    View viewShadow;
    View dialogView;
    AppCompatTextView tvDialogInfos;
    AlertDialog dialogLoading;
    Gson gsonCalendriers;
    Type typeCalendriers;
    Gson gsonSms;
    Type typeSms;
    Gson gsonFamiles;
    Type typeFamiles;
    Gson gsonVacines;
    Type typeVacines;
    List<SmsUser> smsUsers;
    List<Vaccine> vaccines;
    List<Family> families;
    List<CalendrierVaccinal> calendrierVaccinals;

    OpiSmsDatabase opiSmsDatabase;
    PeriodicWorkRequest pwrFamily;
    PeriodicWorkRequest pwrCalendrier;
    PeriodicWorkRequest pwrSms;
    PeriodicWorkRequest pwrVacines;
    LoadVaccinesRight loadVaccinesRight;
    LoadVaccinesMissed loadVaccinesMissed;
    LoadVaccinesIncome loadVaccinesIncome;

    OPISMSApiServices opismsServices;

    Patient gPatient;
    String gPatientID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        bindViews();

        try {
            opismsServices = OPISMSApiClient.getClient(this).create(OPISMSApiServices.class);

            dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_loading_infos_layout, null);
            tvDialogInfos = dialogView.findViewById(R.id.dialog_loading_infos_layout_tv_loading_infos);

            dialogLoading = new AlertDialog.Builder(this)
                    .setView(dialogView)
                    .setCancelable(false)
                    .create();

            opiSmsDatabase = OpiSmsDatabase.getDatabase(ContainerActivity.this);

            gsonCalendriers = new GsonBuilder().serializeNulls().create();
            typeCalendriers = new TypeToken<ArrayList<CalendrierVaccinal>>() {}.getType();

            calendrierVaccinals = new ArrayList<>();

            gsonSms = new GsonBuilder().serializeNulls().create();
            typeSms = new TypeToken<ArrayList<SmsUser>>() {
            }.getType();

            smsUsers = new ArrayList<>();

            gsonFamiles = new GsonBuilder().serializeNulls().create();
            typeFamiles = new TypeToken<ArrayList<Family>>() {}.getType();

            families = new ArrayList<>();

            gsonVacines = new GsonBuilder().serializeNulls().create();
            typeVacines = new TypeToken<ArrayList<Vaccine>>() {}.getType();

            vaccines = new ArrayList<>();

            pwrSms = new PeriodicWorkRequest.Builder(SmsWorker.class, 15, TimeUnit.MINUTES)
                    .build();

            pwrFamily = new PeriodicWorkRequest.Builder(FamilyWorker.class, 15, TimeUnit.MINUTES)
                    .build();

            pwrCalendrier = new PeriodicWorkRequest.Builder(CalendrierWorker.class, 15, TimeUnit.MINUTES)
                    .build();

            WorkManager.getInstance().enqueue(pwrSms);
            WorkManager.getInstance().enqueue(pwrFamily);
            WorkManager.getInstance().enqueue(pwrCalendrier);

            if (SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0").equals("0")) {
                showFragment(LoginFragment.newInstance());
            } else {
                gPatient = opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"));
                showFragment(DashboardFragment.newInstance(gPatient));
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            LogUtils.e("Data => " + data.getDataString());
            LogUtils.e("Path => " + (UriUtils.uri2File(Uri.parse(data.toString())) == null ? "Null" : "Datas"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void bindViews() {
        try {
            gPatientID = SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0");

            flWrapper = findViewById(R.id.activity_container_fl_wrapper);
            ivAppBarAction = findViewById(R.id.activity_container_iv_action_appbar);
            ivAppBarMenu = findViewById(R.id.activity_container_iv_menu_appbar);
            tvAppBarTitle = findViewById(R.id.activity_container_tv_title_appbar);
            rlAppBarContainer = findViewById(R.id.activity_container_rl_appbar);
            viewShadow = findViewById(R.id.activity_container_view_shadow);

            SPUtils.getInstance().remove("paiement", true);

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 225);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    public void loading() {
        dialogLoading.show();
    }


    @Override
    public void onLoginShow() {
        //LogUtils.e("ContainerActivity", "is called");
        rlAppBarContainer.setBackgroundResource(R.color.white);
        viewShadow.setVisibility(View.GONE);
    }


    private void showFragment(final Fragment fragment) {
        if (fragment == null) {
            return;
        }
        // Begin a fragment transaction.
        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();
        // We can also animate the changing of fragment.
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        // Replace current fragment by the new one.
        ft.add(flWrapper.getId(), fragment);
        // Null on the back stack to return on the previous fragment when user
        // press on back button.
        ft.addToBackStack(fragment.getClass().getSimpleName());
        // Commit changes.
        ft.commit();
    }


    private void backFragment(final Fragment fragment) {
        KeyboardUtils.hideSoftInput(this);
        if (fragment == null) {
            return;
        }
        // Begin a fragment transaction.
        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();
        // We can also animate the changing of fragment.
        ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
        // Replace current fragment by the new one.
        ft.remove(fragment);
        // Null on the back stack to return on the previous fragment when user
        // press on back button.
        ft.addToBackStack(null);
        // Commit changes.
        ft.commit();
    }



    @Override
    public void hideRegisterFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void userRegistered() {
        ToastUtils.showLong("Votre compte est créé avec succes. Connectez-vous !");
    }


    @Override
    public void userLogged() {
        gPatient = opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "1"));
        rlAppBarContainer.setVisibility(View.GONE);

        new LoadCalendriers().execute();
        showFragment(DashboardFragment.newInstance(gPatient));
    }


    @Override
    public void unloading() {
        dialogLoading.dismiss();
    }


    @Override
    public void registerAccount() {
        showFragment(RegisterFragment.newInstance("", ""));
    }


    @Override
    public void forgetPasswordAccount() {
        showFragment(ForgetPasswordFragment.newInstance("", ""));
    }


    @Override
    public void reaboClicked() {
        showFragment(ReabonnementFragment.newInstance());
    }


    @Override
    public void showDashboardFragment() {
        rlAppBarContainer.setVisibility(View.GONE);
    }


    @Override
    public void profilClicked() {
        showFragment(ProfilFragment.newInstance());
    }


    @Override
    public void carnetClicked() {
        showFragment(CarnetDashbordFragment.newInstance(gPatientID));
    }


    @Override
    public void smsUserClicked() {
        showFragment(SmsUserFragment.newInstance());
    }


    @Override
    public void myFamilyClicked() {
        showFragment(FamiliesListFragment.newInstance());
    }


    @Override
    public void writeUsClicked() {
        showFragment(WriteUsFragment.newInstance());
    }


    @Override
    public void configClicked() {
        showFragment(UpdatePasswordFragment.newInstance());
    }


    @Override
    public void aboutClicked() {
        showFragment(AboutFragment.newInstance());
    }


    @Override
    public void showProfilFragment() {
    }


    @Override
    public void hideProfilFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void gotoFullscreen(String imageUrl) {
        showFragment(FullscreenImageFragment.newInstance(imageUrl));
    }


    @Override
    public void onBackPressed() {
        ToastUtils.showShort("Utiliser les boutons à cet effet ! Svp");
    }


    @Override
    public void hideSmsUserFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
        LogUtils.e("TAG", "Frg called: " + getSupportFragmentManager().findFragmentById(flWrapper.getId()).getClass().getSimpleName());
    }


    @Override
    public void smsUserLoading() {
        tvDialogInfos.setText("Affichage des sms...");
        dialogLoading.show();
    }


    @Override
    public void smsUserUnloadng() {
        dialogLoading.dismiss();
    }


    @Override
    public void onSmsUserClicked(SmsUser smsUser) {
        showFragment(DetailsSmsUserFragment.newInstance(smsUser));
    }


    @Override
    public void hideDetailsSmsUserFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void hideVaccinesListFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void fetchingVaccinesListLoading() {
        tvDialogInfos.setText("Affichage vaccins...");

        if (!dialogLoading.isShowing()) {
            dialogLoading.show();
        }
    }

    @Override
    public void fetchingFinished() {
        dialogLoading.dismiss();
    }


    @Override
    public void onVaccineClicked(CalendrierVaccinal vaccinal) {
        ToastUtils.showShort("vaccine : " + vaccinal.getVaccinNom());
    }


    @Override
    public void hideFamiliesListFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void fetchingFamiliesLoading() {
        tvDialogInfos.setText("Affichage familles");

        if (!dialogLoading.isShowing()) {
            dialogLoading.show();
        }
    }


    @Override
    public void fetchingFamiliesFinished() {
        dialogLoading.dismiss();
    }

    @Override
    public void hideAboutFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void hideCarnetDashboard() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void vaccineRight(String oPatientID) {
        showFragment(VaccinesRightFragment.newInstance(oPatientID, Integer.parseInt(oPatientID) != Integer.parseInt(gPatientID)));
    }


    @Override
    public void vaccineMissed(String oPatientID) {
        showFragment(VaccinesMissedFragment.newInstance(oPatientID, Integer.parseInt(oPatientID) != Integer.parseInt(gPatientID)));
    }


    @Override
    public void vaccineIncome(String oPatientID) {
        showFragment(VaccinesIncomeFragment.newInstance(oPatientID, Integer.parseInt(oPatientID) != Integer.parseInt(gPatientID)));
    }


    @Override
    public void hideVaccinesRightFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void hideVaccinesMissedFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void startFetchVacinesRight() {
        tvDialogInfos.setText("Liste des vaccins");

        if (!dialogLoading.isShowing()) {
            dialogLoading.show();
        }
    }


    @Override
    public void endFetchVacinesRight() {
        dialogLoading.dismiss();
    }


    @Override
    public void hideReabonnementFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }

    @Override
    public void hideVaccinesIncomeFragment() {
        dismissCurrentFragment();
    }


    private void dismissCurrentFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void startFetchVaccinesIncome() {
        tvDialogInfos.setText("Liste des vaccins");

        if (!dialogLoading.isShowing()) {
            dialogLoading.show();
        }
    }


    @Override
    public void endFetchVaccinesIncome() {
        dialogLoading.dismiss();
    }


    @Override
    public void hideUpdatePasswordFragment() {
        dismissCurrentFragment();
    }


    @Override
    public void startUpdatePass() {
        tvDialogInfos.setText("Modification en cours...");

        if (!dialogLoading.isShowing()) {
            dialogLoading.show();
        }
    }


    @Override
    public void endUpdatePass(int code) {
        dialogLoading.dismiss();

        if (code == 0) {
            dismissCurrentFragment();
        }
    }


    @Override
    public void hideWriteUsFragment() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    @Override
    public void showVaccinePic(String img) {
        /*RelativeLayout rlContainer = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.dialig_image_vaccine, null);
        AppCompatImageView ivVaccine = rlContainer.findViewById(R.id.dialig_image_vaccine_iv_image_vaccine);

        Glide.with(this)
                .asBitmap()
                .load(Base64.decode(img, Base64.DEFAULT))
                .into(ivVaccine);


        new AlertDialog.Builder(this).setView(rlContainer).setCancelable(true).create().show();*/
        gotoFullscreen(img);
    }


    @Override
    public void onFamilyClicked(Family family, int oTypeDetails) {
        if (oTypeDetails == 1)
            showFragment(CarnetDashbordFragment.newInstance(family.getPatientId()));
        else
            showFragment(ProfilFragment.newInstance(family, true));

    }


    @Override
    public void forgetResetLoading(String adress) {
        tvDialogInfos.setText("Reinitialisation du mot de passe");
        loading();
        resetPassword(adress);
    }


    @Override
    public void forgetResetLoginEnd() {
        unloading();
        hideForgetPassword();
    }


    @Override
    public void hideForgetPassword() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }

    @Override
    public void hideFullscreenImage() {
        backFragment(getSupportFragmentManager().findFragmentById(flWrapper.getId()));
    }


    private class LoadCalendriers extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //dialogLoading.dismiss();

            tvDialogInfos.setText("Chargement calendriers...");
            dialogLoading.show();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient clientCalendriers = new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody paramsCalendriers = new FormBody.Builder()
                        .add("patientId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .build();

                Request requestCalendriers = new Request.Builder()
                        .url(UrlBanks.LOAD_USER_CALENDRIERS)
                        .post(paramsCalendriers)
                        .build();

                Response responseCalendriers = clientCalendriers.newCall(requestCalendriers).execute();
                String strCalendriers = responseCalendriers.body().string();

                if (responseCalendriers.isSuccessful()) {
                    JSONObject jsonCalendriers = new JSONObject(strCalendriers);

                    if (jsonCalendriers.getInt("code") == 0) {
                        calendrierVaccinals.clear();
                        calendrierVaccinals.addAll((ArrayList<CalendrierVaccinal>) gsonCalendriers.fromJson(jsonCalendriers.getJSONArray("data").toString(), typeCalendriers));

                        if (opiSmsDatabase.calendrierVaccinalDao().getCalendrierVaccinaux().size() == 0) {
                            for (CalendrierVaccinal calendrierVaccinal : calendrierVaccinals) {
                                opiSmsDatabase.calendrierVaccinalDao().createCalendrierVaccinal(calendrierVaccinal);
                            }
                        }
                    } else {
                        code = jsonCalendriers.getInt("code");
                    }
                } else {
                    code = 4;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                code = 4;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            //dialogLoading.dismiss();
            new LoadSms().execute();
        }
    }


    private class LoadSms extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            tvDialogInfos.setText("Chargement messages...");

            if (!dialogLoading.isShowing()) {
                dialogLoading.show();
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient clientSms = new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody paramsSms = new FormBody.Builder()
                        .add("patientId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .build();

                Request requestSms = new Request.Builder()
                        .url(UrlBanks.LOAD_USER_SMS)
                        .post(paramsSms)
                        .build();

                Response responseSms = clientSms.newCall(requestSms).execute();
                String strSms = responseSms.body().string();

                if (responseSms.isSuccessful()) {
                    JSONObject jsonSms = new JSONObject(strSms);

                    if (jsonSms.getInt("code") == 0) {
                        smsUsers.clear();
                        smsUsers.addAll((ArrayList<SmsUser>) gsonSms.fromJson(jsonSms.getJSONArray("data").toString(), typeSms));

                        if (opiSmsDatabase.smsUserDao().getSms().size() == 0) {
                            for (SmsUser smsUser : smsUsers) {
                                opiSmsDatabase.smsUserDao().createUserSms(smsUser);
                            }
                        }
                    } else {
                        code = jsonSms.getInt("code");
                    }
                } else {
                    code = 4;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                code = 4;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            //dialogLoading.dismiss();

            /*if (integer != 0) {
                new LoadSms().execute();
            }*/

            new LoadFamiles().execute();
        }
    }


    private class LoadFamiles extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            tvDialogInfos.setText("Chargement familles...");

            if (!dialogLoading.isShowing()) {
                dialogLoading.show();
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient clientFamiles = new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody paramsFamiles = new FormBody.Builder()
                        .add("patientId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .build();

                Request requestFamiles = new Request.Builder()
                        .url(UrlBanks.LOAD_USER_FAMILIES)
                        .post(paramsFamiles)
                        .build();

                Response responseFamiles = clientFamiles.newCall(requestFamiles).execute();
                String strFamiles = responseFamiles.body().string();

                if (responseFamiles.isSuccessful()) {
                    JSONObject jsonFamiles = new JSONObject(strFamiles);

                    if (jsonFamiles.getInt("code") == 0) {
                        families.clear();
                        families.addAll((ArrayList<Family>) gsonFamiles.fromJson(jsonFamiles.getJSONArray("data").toString(), typeFamiles));

                        if (opiSmsDatabase.familyDao().getFamilies().size() == 0) {
                            for (Family family : families) {
                                opiSmsDatabase.familyDao().createFamily(family);
                            }
                        }
                    } else {
                        code = jsonFamiles.getInt("code");
                    }
                } else {
                    code = 4;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                code = 4;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            //dialogLoading.dismiss();
            //new LoadFamiles().execute();

            /*if (integer != 0) {
                new LoadFamiles().execute();
            }*/


            loadVaccinesRight = null;
            loadVaccinesRight = new LoadVaccinesRight();
            loadVaccinesRight.execute();
        }
    }


    private class LoadVaccinesRight extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            tvDialogInfos.setText("Chargement vaccins...");

            if (!dialogLoading.isShowing()) {
                dialogLoading.show();
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient rightClient = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody rightForms = new FormBody.Builder()
                        .add("patId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .build();

                Request rightRequest = new Request.Builder()
                        .url(UrlBanks.USER_VACCINE_RIGHT_LIST)
                        .method("POST", rightForms)
                        .build();

                Response rightResponse = rightClient.newCall(rightRequest).execute();
                String rightString = rightResponse.body().string();

                LogUtils.e("TAG", rightString);

                if (rightResponse.isSuccessful()) {
                    JSONObject rightJson = new JSONObject(rightString);

                    if (rightJson.getInt("statut") == 1) {
                        code = 0;
                        vaccines.clear();
                        vaccines.addAll((ArrayList<Vaccine>) gsonVacines.fromJson(rightJson.getJSONArray("data").toString(), typeVacines));

                        for (Vaccine vac : vaccines) {
                            opiSmsDatabase.vaccineDao().createVaccine(vac);
                        }
                    } else {
                        code = 1;
                    }
                } else {
                    code = 2;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                code = 3;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            LogUtils.e("TAG", "Vaccine => " + integer);

            switch (integer) {
                case 0:
                case 1:

                    //loadVaccinesRight.cancel(true);

                    loadVaccinesMissed = new LoadVaccinesMissed();
                    loadVaccinesMissed.execute();
                    break;

                case 2:
                case 3:
                    loadVaccinesRight = null;
                    loadVaccinesRight = new LoadVaccinesRight();
                    loadVaccinesRight.execute();
                    break;
            }
        }
    }


    private class LoadVaccinesMissed extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //tvDialogInfos.setText("Chargement vaccins...");

            if (!dialogLoading.isShowing()) {
                dialogLoading.show();
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient missedClient = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody missedForms = new FormBody.Builder()
                        .add("patId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .build();

                Request missedRequest = new Request.Builder()
                        .url(UrlBanks.USER_VACCINE_MISSED_LIST)
                        .method("POST", missedForms)
                        .build();
                Response missedResponse = missedClient.newCall(missedRequest).execute();
                String missedString = missedResponse.body().string();

                if (missedResponse.isSuccessful()) {
                    JSONObject missedJson = new JSONObject(missedString);

                    if (missedJson.getInt("statut") == 1) {
                        code = 0;
                        vaccines.clear();
                        vaccines.addAll((ArrayList<Vaccine>) gsonVacines.fromJson(missedJson.getJSONArray("data").toString(), typeVacines));

                        for (Vaccine vac : vaccines) {
                            opiSmsDatabase.vaccineDao().createVaccine(vac);
                        }
                    } else {
                        code = 1;
                    }
                } else {
                    code = 2;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                code = 3;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            switch (integer) {
                case 0:
                case 1:
                    //loadVaccinesMissed.cancel(true);

                    loadVaccinesIncome = new LoadVaccinesIncome();
                    loadVaccinesIncome.execute();
                    break;

                case 2:
                case 3:
                    loadVaccinesMissed = new LoadVaccinesMissed();
                    loadVaccinesMissed.execute();
                    break;
            }
        }
    }


    private class LoadVaccinesIncome extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //tvDialogInfos.setText("Chargement vaccins...");

            if (!dialogLoading.isShowing()) {
                dialogLoading.show();
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient incomeClient = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody incomeForms = new FormBody.Builder()
                        .add("patId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .build();

                Request incomeRequest = new Request.Builder()
                        .url(UrlBanks.USER_VACCINE_INCOME_LIST)
                        .method("POST", incomeForms)
                        .build();

                Response incomeResponse = incomeClient.newCall(incomeRequest).execute();
                String incomeString = incomeResponse.body().string();

                LogUtils.e("TAG", incomeString);

                if (incomeResponse.isSuccessful()) {
                    JSONObject incomeJson = new JSONObject(incomeString);

                    if (incomeJson.getInt("statut") == 1) {
                        code = 0;
                        vaccines.clear();
                        vaccines.addAll((ArrayList<Vaccine>) gsonVacines.fromJson(incomeJson.getJSONArray("data").toString(), typeVacines));

                        for (Vaccine vac : vaccines) {
                            opiSmsDatabase.vaccineDao().createVaccine(vac);
                        }
                    } else {
                        code = 1;
                    }
                } else {
                    code = 2;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                code = 3;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            switch (integer) {
                case 0:
                case 1:
                    loadVaccinesRight = null;
                    loadVaccinesMissed = null;
                    loadVaccinesIncome = null;

                    dialogLoading.dismiss();
                    break;

                case 2:
                case 3:
                    loadVaccinesIncome = new LoadVaccinesIncome();
                    loadVaccinesIncome.execute();
                    break;
            }
        }
    }


    void resetPassword(String email) {
        opismsServices.resetPassword(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<CommonResponse>() {
                    @Override
                    public void onSuccess(CommonResponse commonResponse) {
                        LogUtils.json(commonResponse);

                        if (commonResponse.getCode() == 1) {
                            forgetResetLoginEnd();
                        } else {
                            unloading();
                        }

                        ToastUtils.showShort(commonResponse.getMsg());
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e(e.getMessage());
                    }
                });
    }

}
