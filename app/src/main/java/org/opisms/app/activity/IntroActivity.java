package org.opisms.app.activity;

import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatImageView;

import com.blankj.utilcode.constant.TimeConstants;
import com.blankj.utilcode.util.ActivityUtils;

import org.opisms.app.R;

public class IntroActivity extends AppCompatActivity {
	

	AppCompatImageView ivLogo;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);

		bindViews();

		try {
			new CountDownTimer(3 * TimeConstants.SEC, TimeConstants.SEC) {
				@Override
				public void onTick(long millisUntilFinished) {

				}

				@Override
				public void onFinish() {
					ActivityUtils.startActivity(ContainerActivity.class, R.anim.enter_from_right, R.anim.exit_to_left);
					finish();
				}
			}.start();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	
	private void bindViews() {
		try {
			ivLogo = findViewById(R.id.activity_intro_iv_logo);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
}
