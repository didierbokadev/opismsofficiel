package org.opisms.app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.os.Bundle;
import android.view.View;

import org.opisms.app.R;

import butterknife.OnClick;

public class PrivacyActivity extends AppCompatActivity {


    AppCompatImageView imagePrivacy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

        imagePrivacy = findViewById(R.id.fragment_privacy_image_close);
        imagePrivacy.setOnClickListener(v -> finish());
    }
}
