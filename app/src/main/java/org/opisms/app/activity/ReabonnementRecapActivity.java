package org.opisms.app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.JavascriptInterface;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.cinetpay.androidsdk.CinetPayActivity;
import com.cinetpay.androidsdk.CinetPayWebAppInterface;

import org.opisms.app.R;
import org.opisms.app.model.CinetPayResponseModel;
import org.opisms.app.utils.UrlBanks;

import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ReabonnementRecapActivity extends CinetPayActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        String apiKey = intent.getStringExtra(KEY_API_KEY);
        String business = intent.getStringExtra(KEY_SITE_ID);
        String transactionId = intent.getStringExtra(KEY_TRANSACTION_ID);
        String cinetpayCallback = intent.getStringExtra(KEY_NOTIFY_URL);
        int amountToPay = intent.getIntExtra(KEY_AMOUNT, 0);
        //  int amountToPay = 100;
        String currency = intent.getStringExtra(KEY_CURRENCY);
        String description = intent.getStringExtra(KEY_DESCRIPTION);
        String channels = intent.getStringExtra(KEY_CHANNELS);


        if (channels.equals("CREDIT_CARD")) {
            String mobileMoney = intent.getStringExtra(KEY_CUSTOMER_PHONE_NUMBER);
            String customerName = intent.getStringExtra(KEY_CUSTOMER_NAME);
            String customerSurname = intent.getStringExtra(KEY_CUSTOMER_SURNAME);
            String customerEmail = intent.getStringExtra(KEY_CUSTOMER_EMAIL);
            String customerAddress = intent.getStringExtra(KEY_CUSTOMER_ADDRESS);
            String customerCity = intent.getStringExtra(KEY_CUSTOMER_CITY);
            String customerCountry = intent.getStringExtra(KEY_CUSTOMER_COUNTRY);
            String customerZipCode = intent.getStringExtra(KEY_CUSTOMER_ZIP_CODE);

            mWebView.addJavascriptInterface(
                    new CinetPayWebAppInterface(
                            this,
                            apiKey,        // obligatoire
                            business,        // obligatoire
                            transactionId, // obligatoire
                            amountToPay,// obligatoire
                            currency,       // obligatoire
                            description)
                    {
                        @Override
                        @JavascriptInterface
                        public void onResponse(String data) {
                            SPUtils.getInstance().put("paiement", data);
                            finish();
                        }

                        @Override
                        @JavascriptInterface
                        public void onError(String data) {
                            SPUtils.getInstance().put("paiement", data);
                            finish();
                        }
                    }       .setNotifyUrl(cinetpayCallback)
                            .setChannels(channels)
                            .setCustomerName(customerName)
                            .setCustomerSurname(customerSurname)
                            .setCustomerEmail(customerEmail)
                            .setCustomerAddress(customerAddress)
                            .setCustomerPhoneNumber(mobileMoney)
                            .setCustomerCity(customerCity)
                            .setCustomerCountry(customerCountry)
                            .setCustomerZipCode(customerZipCode),
                    "Android"
            );
        } else {
            mWebView.addJavascriptInterface(
                    new CinetPayWebAppInterface(
                            this,
                            apiKey,        // obligatoire
                            business,        // obligatoire
                            transactionId, // obligatoire
                            amountToPay,// obligatoire
                            currency,       // obligatoire
                            description)
                    {
                        @Override
                        @JavascriptInterface
                        public void onResponse(String data) {
                            SPUtils.getInstance().put("paiement", data);
                            finish();
                        }


                        @Override
                        @JavascriptInterface
                        public void onError(String data) {
                            SPUtils.getInstance().put("paiement", data);
                            finish();
                        }
                    }
                    .setNotifyUrl(cinetpayCallback),
                    "Android"
            );
        }
    }
}
