package org.opisms.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.opisms.app.R;
import org.opisms.app.model.Family;

import java.util.List;

/**
 * Created by Cedric.BOKA on 14/02/2019.
 */

@SuppressWarnings("ALL")
public class FamilyAdapter extends RecyclerView.Adapter<FamilyAdapter.FamilyHolder> {


    private Context ctx;
    private List<Family> families;
    private OnFamilyAdapterListener mListener;


    public FamilyAdapter(Context ctx, List<Family> families) {
        this.ctx = ctx;
        this.families = families;

        if (ctx instanceof OnFamilyAdapterListener) {
            mListener = (OnFamilyAdapterListener) ctx;
        } else {
            throw new RuntimeException(ctx.toString() +" mus t implement OnFamilyAdapterListener");
        }
    }


    @NonNull
    @Override
    public FamilyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View viewFamilies = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.family_items_list, viewGroup, false);
        return new FamilyHolder(viewFamilies);
    }


    @Override
    public void onBindViewHolder(@NonNull FamilyHolder familyHolder, int i) {
        try {
            Family family = families.get(familyHolder.getAdapterPosition());

            familyHolder.tvNomPrenoms.setText(family.getNomPatient() + " " + family.getPrenomPatient());
            familyHolder.tvDateNaissance.setText(DateTime.parse(family.getPatientNaissance(), DateTimeFormat.forPattern("yyyy-MM-dd")).toString("dd/MM/yyyy"));

            if (family.getPatientSexe().equals("M")) {
                familyHolder.ivSexeIndicator.setImageResource(R.drawable.ic_manager);
            } else {
                familyHolder.ivSexeIndicator.setImageResource(R.drawable.ic_woman);
            }

            familyHolder.rlContainer.setOnClickListener(v -> {
                mListener.onFamilyClicked(family, 1);
            });



            familyHolder.ivProfile.setOnClickListener(v -> {
                mListener.onFamilyClicked(family, 2);
            });
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    public int getItemCount() {
        return families.size();
    }


    static class FamilyHolder extends ViewHolder {

        AppCompatTextView tvNomPrenoms;
        AppCompatTextView tvDateNaissance;
        AppCompatImageView ivSexeIndicator;
        AppCompatImageView ivProfile;
        RelativeLayout rlContainer;

        public FamilyHolder(@NonNull View itemView) {
            super(itemView);

            try {
                tvDateNaissance = itemView.findViewById(R.id.family_items_list_tv_family_naissance);
                tvNomPrenoms = itemView.findViewById(R.id.family_items_list_tv_family_nom_prenom);
                ivSexeIndicator = itemView.findViewById(R.id.family_items_list_iv_family);
                ivProfile = itemView.findViewById(R.id.family_items_list_iv_family_profile);
                rlContainer = itemView.findViewById(R.id.family_items_list_rl_container);
            } catch (Exception ex) {
             throw new RuntimeException(ex);
            }
        }
    }


    public interface OnFamilyAdapterListener {
        void onFamilyClicked(Family family, int iTypeDetails);
    }

}
