package org.opisms.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.opisms.app.R;
import org.opisms.app.model.SmsUser;

import java.util.List;

/**
 * Created by home on 13/02/2019.
 */

@SuppressWarnings("ALL")
public class SmsUserAdapter extends RecyclerView.Adapter<SmsUserAdapter.SmsUserHolder> {


    private Context ctx;
    private List<SmsUser> smsUsers;
    private OnSmsUserAdapterListener mListener;


    public SmsUserAdapter(Context ctx, List<SmsUser> smsUsers) {
        this.ctx = ctx;
        this.smsUsers = smsUsers;

        if (ctx instanceof OnSmsUserAdapterListener) {
            mListener = (OnSmsUserAdapterListener) ctx;
        } else {
            throw new RuntimeException(ctx.toString() +" mus t implement OnSmsUserAdapterListener");
        }
    }


    @NonNull
    @Override
    public SmsUserHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View viewSmsUsers = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sms_items_list, viewGroup, false);
        return new SmsUserHolder(viewSmsUsers);
    }


    @Override
    public void onBindViewHolder(@NonNull SmsUserHolder smsUserHolder, int i) {
        try {
            final SmsUser smsUser = smsUsers.get(smsUserHolder.getAdapterPosition());

            smsUserHolder.tvSnippetSms.setText(smsUser.getSmsBody());

            if (smsUser.getSmsDateEnvoi().equals("0000-00-00 00:00:00")) {
                smsUserHolder.tvDateSms.setText("Inconnu");
            } else {
                smsUserHolder.tvDateSms.setText(DateTime.parse(smsUser.getSmsDateEnvoi(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toString("dd/MM/yyyy 'à' HH:mm:ss" ));
            }

            smsUserHolder.rlConatiner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onSmsUserClicked(smsUser);
                }
            });
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    public int getItemCount() {
        return smsUsers.size();
    }


    static class SmsUserHolder extends RecyclerView.ViewHolder {

        AppCompatTextView tvSnippetSms;
        AppCompatTextView tvDateSms;
        RelativeLayout rlConatiner;

        public SmsUserHolder(@NonNull View itemView) {
            super(itemView);

            try {
                tvDateSms = itemView.findViewById(R.id.sms_items_list_tv_sms_date);
                tvSnippetSms = itemView.findViewById(R.id.sms_items_list_tv_sms_snippet);
                rlConatiner = itemView.findViewById(R.id.sms_items_list_rl_container);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public interface OnSmsUserAdapterListener {
        void onSmsUserClicked(SmsUser smsUser);
    }

}
