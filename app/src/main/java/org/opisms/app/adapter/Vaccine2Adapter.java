package org.opisms.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import org.opisms.app.R;
import org.opisms.app.model.Vaccine;

import java.util.List;

/**
 * Created by Cedric.BOKA on 14/02/2019.
 */

@SuppressWarnings("ALL")
public class Vaccine2Adapter extends RecyclerView.Adapter<Vaccine2Adapter.VaccineHolder> {


    private Context ctx;
    private List<Vaccine> vaccinalList;
    OnVaccine2AdapterListener mListener;


    public Vaccine2Adapter(Context ctx, List<Vaccine> vaccinalList) {
        this.ctx = ctx;
        this.vaccinalList = vaccinalList;

        if (ctx instanceof OnVaccine2AdapterListener) {
            mListener = (OnVaccine2AdapterListener) ctx;
        } else {
            throw new RuntimeException(ctx.toString() + " must to implement OnVaccineAdapterListener");
        }
    }


    @NonNull
    @Override
    public VaccineHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View viewVaccines = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vaccine_items_list, viewGroup, false);
        return new VaccineHolder(viewVaccines);
    }


    @Override
    public void onBindViewHolder(@NonNull final VaccineHolder vaccineHolder, int i) {
        try {
            final Vaccine vaccine = vaccinalList.get(vaccineHolder.getAdapterPosition());

            vaccineHolder.tvVaccineLabel.setText(vaccine.getVacLabel());
            vaccineHolder.tvVaccineCenter.setText(vaccine.getVacCenter());
            vaccineHolder.tvVaccineLot.setText(vaccine.getVacLot());
            vaccineHolder.tvVaccinePresence.setText(vaccine.getVacDatePresence());
            vaccineHolder.tvVaccineRappel.setText(vaccine.getVacDateRappel());

            vaccineHolder.ivVaccinePic.setImageResource(0);

            if (vaccine.getVacImage() == null) {
                switch (vaccine.getVacType()) {
                    case "1" :
                        vaccineHolder.ivVaccinePic.setImageResource(R.drawable.ic_syringe_right_vac);
                        break;

                    case "2" :
                        vaccineHolder.ivVaccinePic.setImageResource(R.drawable.ic_syringe_missed_vac_blue);
                        break;

                    case "3" :
                        vaccineHolder.ivVaccinePic.setImageResource(R.drawable.ic_vaccination_blue);
                        break;
                }
            } else {
                if (vaccine.getVacImage().length() > 0) {
                    Glide.with(ctx)
                            .asBitmap()
                            .load(Base64.decode(vaccine.getVacImage(), Base64.DEFAULT))
                            .into(vaccineHolder.ivVaccinePic);
                } else {
                    switch (vaccine.getVacType()) {
                        case "1" :
                            vaccineHolder.ivVaccinePic.setImageResource(R.drawable.ic_syringe_right_vac);
                            break;

                        case "2" :
                            vaccineHolder.ivVaccinePic.setImageResource(R.drawable.ic_syringe_missed_vac_blue);
                            break;

                        case "3" :
                            vaccineHolder.ivVaccinePic.setImageResource(R.drawable.ic_vaccination_blue);
                            break;
                    }
                }
            }

            vaccineHolder.ivVaccinePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (vaccine.getVacImage() != null) {
                        if (vaccine.getVacImage().length() > 0) {
                            mListener.showVaccinePic(vaccine.getVacImage());
                        }
                    }
                }
            });
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    public int getItemCount() {
        return vaccinalList.size();
    }


    static class VaccineHolder extends RecyclerView.ViewHolder {

        AppCompatTextView tvVaccineLabel;
        AppCompatTextView tvVaccineCenter;
        AppCompatTextView tvVaccineLot;
        AppCompatTextView tvVaccinePresence;
        AppCompatTextView tvVaccineRappel;
        AppCompatImageView ivVaccinePic;
        RelativeLayout rlContainer;

        public VaccineHolder(@NonNull View itemView) {
            super(itemView);

            rlContainer = itemView.findViewById(R.id.vaccine_items_list_rl_container);
            tvVaccineCenter = itemView.findViewById(R.id.vaccine_items_list_tv_vaccine_center);
            ivVaccinePic = itemView.findViewById(R.id.vaccine_items_list_iv_syringe);
            tvVaccineLabel = itemView.findViewById(R.id.vaccine_items_list_tv_vaccine_label);
            tvVaccinePresence = itemView.findViewById(R.id.vaccine_items_list_tv_vaccine_date_presence);
            tvVaccineRappel = itemView.findViewById(R.id.vaccine_items_list_tv_vaccine_date_rappel);
            tvVaccineLot = itemView.findViewById(R.id.vaccine_items_list_tv_vaccine_lot);
        }
    }


    public interface OnVaccine2AdapterListener {
        void showVaccinePic(String img);
    }
}
