package org.opisms.app.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import org.opisms.app.R;
import org.opisms.app.model.CalendrierVaccinal;

import java.util.List;

/**
 * Created by Cedric.BOKA on 14/02/2019.
 */

@SuppressWarnings("ALL")
public class VaccineAdapter extends RecyclerView.Adapter<VaccineAdapter.VaccineHolder> {


    private Context ctx;
    private List<CalendrierVaccinal> vaccinalList;
    OnVaccineAdapterListener mListener;


    public VaccineAdapter(Context ctx, List<CalendrierVaccinal> vaccinalList) {
        this.ctx = ctx;
        this.vaccinalList = vaccinalList;

        if (ctx instanceof OnVaccineAdapterListener) {
            mListener = (OnVaccineAdapterListener) ctx;
        } else {
            throw new RuntimeException(ctx.toString() + " must to implement OnVaccineAdapterListener");
        }
    }


    @NonNull
    @Override
    public VaccineHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View viewVaccines = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vaccine_items_list, viewGroup, false);
        return new VaccineHolder(viewVaccines);
    }


    @Override
    public void onBindViewHolder(@NonNull final VaccineHolder vaccineHolder, int i) {
        try {
            final CalendrierVaccinal calendrierVaccinal = vaccinalList.get(vaccineHolder.getAdapterPosition());

            vaccineHolder.tvVaccineLabel.setText(calendrierVaccinal.getVaccinNom());
            vaccineHolder.tvVaccineCenter.setText(calendrierVaccinal.getCentreNom());

            vaccineHolder.rlContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onVaccineClicked(calendrierVaccinal);
                }
            });
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    public int getItemCount() {
        return vaccinalList.size();
    }


    static class VaccineHolder extends RecyclerView.ViewHolder {

        AppCompatTextView tvVaccineLabel;
        AppCompatTextView tvVaccineCenter;
        RelativeLayout rlContainer;

        public VaccineHolder(@NonNull View itemView) {
            super(itemView);

            try {
                rlContainer = itemView.findViewById(R.id.vaccine_items_list_rl_container);
                tvVaccineCenter = itemView.findViewById(R.id.vaccine_items_list_tv_vaccine_center);
                tvVaccineLabel = itemView.findViewById(R.id.vaccine_items_list_tv_vaccine_label);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }


    public interface OnVaccineAdapterListener {
        void onVaccineClicked(CalendrierVaccinal vaccinal);
    }
}
