package org.opisms.app.datas.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import org.opisms.app.model.CalendrierVaccinal;

import java.util.List;

/**
 * Created by Cedric.BOKA on 05/02/2019.
 */

@SuppressWarnings("ALL")
@Dao
public interface CalendrierVaccinalDao {

    // Insert calendrier infos
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createCalendrierVaccinal(CalendrierVaccinal calendrierVaccinal);

    @Transaction
    @Query("SELECT * FROM calendrier_vaccinal WHERE patient_id = :pPatientId")
    List<CalendrierVaccinal> getCalendrierVaccinauxByPatientId(String pPatientId);

    @Transaction
    @Query("SELECT * FROM calendrier_vaccinal WHERE patient_id = :pCalId")
    List<CalendrierVaccinal> getCalendrierVaccinauxById(String pCalId);

    @Transaction
    @Query("SELECT * FROM calendrier_vaccinal")
    List<CalendrierVaccinal> getCalendrierVaccinaux();


}
