package org.opisms.app.datas.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import org.opisms.app.model.Family;

import java.util.List;

/**
 * Created by Cedric.BOKA on 05/02/2019.
 */

@SuppressWarnings("ALL")
@Dao
public interface FamilyDao {

    // Insert family infos
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createFamily(Family family);

    @Transaction
    @Query("SELECT * FROM family WHERE patient_id = :pPatientId")
    List<Family> getFamiliesByPatientIdd(String pPatientId);

    @Transaction
    @Query("SELECT * FROM family")
    List<Family> getFamilies();

    @Transaction
    @Query("DELETE FROM family")
    void deleteAll();
}
