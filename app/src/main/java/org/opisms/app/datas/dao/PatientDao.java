package org.opisms.app.datas.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import org.opisms.app.model.Patient;

/**
 * Created by Cedric.BOKA on 05/02/2019.
 */

@SuppressWarnings("ALL")
@Dao
public interface PatientDao {


    // Insert patient infos
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createPatient(Patient patient);


    @Transaction
    @Query("SELECT * FROM patient WHERE patient_id = :pPatientId")
    Patient getPatientById(String pPatientId);


    @Query("DELETE FROM patient WHERE patient_id = :patId")
    void disconnectUser(String patId);

}
