package org.opisms.app.datas.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import org.opisms.app.model.SmsUser;

import java.util.List;

/**
 * Created by Cedric.BOKA on 05/02/2019.
 */

@SuppressWarnings("ALL")
@Dao
public interface SmsUserDao {

    // Insert sms infos
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createUserSms(SmsUser smsUser);

    @Transaction
    @Query("SELECT * FROM sms_user WHERE sms_id = :pSmsId")
    List<SmsUser> getSmsById(String pSmsId);

    @Transaction
    @Query("SELECT * FROM sms_user")
    List<SmsUser> getSms();

    @Transaction
    @Query("DELETE FROM sms_user")
    void deleteAll();
}
