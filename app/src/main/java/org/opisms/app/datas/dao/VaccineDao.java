package org.opisms.app.datas.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import org.opisms.app.model.Vaccine;

import java.util.List;

/**
 * Created by Cedric.BOKA on 05/02/2019.
 */

@SuppressWarnings("ALL")
@Dao
public interface VaccineDao {


    // Insert patient infos
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createVaccine(Vaccine vaccine);


    @Transaction
    @Query("SELECT * FROM vaccine WHERE vacType = :type")
    List<Vaccine> getVaccinesByType(String type);


    @Transaction
    @Query("SELECT * FROM vaccine")
    List<Vaccine> getVaccines();


}
