package org.opisms.app.datas.database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import org.opisms.app.datas.dao.CalendrierVaccinalDao;
import org.opisms.app.datas.dao.FamilyDao;
import org.opisms.app.datas.dao.PatientDao;
import org.opisms.app.datas.dao.SmsUserDao;
import org.opisms.app.datas.dao.VaccineDao;
import org.opisms.app.model.CalendrierVaccinal;
import org.opisms.app.model.Family;
import org.opisms.app.model.Patient;
import org.opisms.app.model.SmsUser;
import org.opisms.app.model.Vaccine;

/**
 * Created by Cedric.BOKA on 05/02/2019.
 */

@SuppressWarnings("ALL")
@Database(
        entities = {
                Patient.class,
                CalendrierVaccinal.class,
                SmsUser.class,
                Family.class,
                Vaccine.class
        },
        version = 1,
        exportSchema = false
)
public abstract class OpiSmsDatabase extends RoomDatabase {


    private static OpiSmsDatabase INSTANCE;
    public abstract PatientDao patientDao();
    public abstract CalendrierVaccinalDao calendrierVaccinalDao();
    public abstract SmsUserDao smsUserDao();
    public abstract FamilyDao familyDao();
    public abstract VaccineDao vaccineDao();


    public static OpiSmsDatabase getDatabase(Context context) {
        try {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, OpiSmsDatabase.class, "alivedigit.db")
                        // recreate the database if necessary
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build();
            }
            return INSTANCE;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
