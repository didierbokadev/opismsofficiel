package org.opisms.app.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.opisms.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


@SuppressWarnings("ALL")
public class CarnetDashbordFragment extends Fragment {


    @BindView(R.id.fragment_carnet_dashboard_ll_vaccine_right_container)
    LinearLayout llVaccineRight;
    @BindView(R.id.fragment_carnet_dashboard_ll_vaccine_missed)
    LinearLayout llVaccineMissed;
    @BindView(R.id.fragment_carnet_dashboard_ll_vaccine_incoming)
    LinearLayout llVaccineIncome;
    @BindView(R.id.fragment_carnet_dashboard_iv_back)
    AppCompatImageView ivBack;
    Unbinder unbinder;
    private OnCarnetDashbordFragmentListener mListener;
    String gPatientID = "1";


    public CarnetDashbordFragment() {
        // Required empty public constructor
    }


    public static CarnetDashbordFragment newInstance(String param1, String param2) {
        CarnetDashbordFragment fragment = new CarnetDashbordFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public static CarnetDashbordFragment newInstance() {
        CarnetDashbordFragment fragment = new CarnetDashbordFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public static CarnetDashbordFragment newInstance(String patientID) {
        CarnetDashbordFragment fragment = new CarnetDashbordFragment();
        Bundle args = new Bundle();
        args.putString("patientID", patientID);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            gPatientID = getArguments().getString("patientID", "0");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_carnet_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCarnetDashbordFragmentListener) {
            mListener = (OnCarnetDashbordFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnCarnetDashbordFragmentListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fragment_carnet_dashboard_ll_vaccine_right_container)
    public void onLlVaccineRightClicked() {
        mListener.vaccineRight(gPatientID);
    }


    @OnClick(R.id.fragment_carnet_dashboard_iv_back)
    public void onBack() {
        mListener.hideCarnetDashboard();
    }


    @OnClick(R.id.fragment_carnet_dashboard_ll_vaccine_missed)
    public void onLlVaccineMissedClicked() {
        mListener.vaccineMissed(gPatientID);
    }


    @OnClick(R.id.fragment_carnet_dashboard_ll_vaccine_incoming)
    public void onLlVaccineIncomeClicked() {
        mListener.vaccineIncome(gPatientID);
    }


    public interface OnCarnetDashbordFragmentListener {
        void hideCarnetDashboard();
        void vaccineRight(String iPatientID);
        void vaccineMissed(String iPatientID);
        void vaccineIncome(String iPatientID);
    }
}
