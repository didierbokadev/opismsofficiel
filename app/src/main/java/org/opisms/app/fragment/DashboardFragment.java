package org.opisms.app.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;

import org.opisms.app.R;
import org.opisms.app.activity.IntroActivity;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.Patient;
import org.opisms.app.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

@SuppressWarnings("ALL")
public class DashboardFragment extends Fragment {


    @BindView(R.id.fragment_dashboard_ll_profil_container)
    LinearLayout llProfilContainer;
    @BindView(R.id.fragment_dashboard_ll_carnet_container)
    LinearLayout llCarnetContainer;
    @BindView(R.id.fragment_dashboard_ll_sms_container)
    LinearLayout llSmsContainer;
    @BindView(R.id.fragment_dashboard_ll_famille_container)
    LinearLayout llFamilleContainer;
    @BindView(R.id.fragment_dashboard_ll_reabo_container)
    LinearLayout llReaboContainer;
    @BindView(R.id.fragment_dashboard_ll_ecrire_container)
    LinearLayout llEcrireContainer;
    @BindView(R.id.fragment_dashboard_ll_params_container)
    LinearLayout llParamsContainer;
    @BindView(R.id.image_logout)
    AppCompatImageView imageLogout;
    @BindView(R.id.fragment_dashboard_ll_propos_container)
    LinearLayout llProposContainer;
    @BindView(R.id.fragment_dashboard_tv_welcome_title)
    AppCompatTextView tvWelcomeTitle;
    Unbinder unbinder;
    private OnDashboardFragmentListener mListener;
    Patient patient;
    OpiSmsDatabase opiSmsDatabase;


    public DashboardFragment() {
    }


    public static DashboardFragment newInstance(Patient pPatient) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putParcelable("patientInfos", pPatient);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            patient = getArguments().getParcelable("patientInfos");

            LogUtils.e("Datas is passeed");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDashboardFragmentListener) {
            mListener = (OnDashboardFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnDashboardFragmentListener");
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        opiSmsDatabase = OpiSmsDatabase.getDatabase(getActivity());

        if (patient != null) {
            tvWelcomeTitle.setText("Bienvenu(e) " + patient.getPatNom());
        } else {
            tvWelcomeTitle.setText("Bienvenu(e)");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fragment_dashboard_ll_profil_container)
    public void onLlProfilContainerClicked() {
        mListener.profilClicked();
    }


    @OnClick(R.id.fragment_dashboard_ll_carnet_container)
    public void onLlCarnetContainerClicked() {
        if (Integer.parseInt(opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID)).getPatFormuleId()) != 3
                && Integer.parseInt(opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID)).getPatFormuleId()) != 10) {
            new AlertDialog.Builder(getActivity()).setMessage("Votre profile ne vous permet pas d’accéder à ce menu").create().show();
            return;
        }
        mListener.carnetClicked();
    }


    @OnClick(R.id.fragment_dashboard_ll_sms_container)
    public void onLlSmsContainerClicked() {
        if (Integer.parseInt(opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID)).getPatFormuleId()) != 3
                && Integer.parseInt(opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID)).getPatFormuleId()) != 10) {
            new AlertDialog.Builder(getActivity()).setMessage("Votre profile ne vous permet pas d’accéder à ce menu").create().show();
            return;
        }
        mListener.smsUserClicked();
    }


    @OnClick(R.id.fragment_dashboard_ll_famille_container)
    public void onLlFamilleContainerClicked() {
        if (Integer.parseInt(opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID)).getPatFormuleId()) != 3
                && Integer.parseInt(opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID)).getPatFormuleId()) != 10) {
            new AlertDialog.Builder(getActivity()).setMessage("Votre profile ne vous permet pas d’accéder à ce menu").create().show();
            return;
        }

        mListener.myFamilyClicked();
    }


    @OnClick(R.id.image_logout)
    public void onLogOutClicked() {
        OpiSmsDatabase.getDatabase(getActivity()).patientDao()
                .disconnectUser(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"));

        LogUtils.e("UserId: " + SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"));
        SPUtils.getInstance(Constants.USER_PREFS).clear();

        ActivityUtils.startActivity(IntroActivity.class, R.anim.enter_from_left, R.anim.exit_to_right);
    }


    @OnClick(R.id.fragment_dashboard_ll_reabo_container)
    public void onLlReaboContainerClicked() {
        mListener.reaboClicked();
    }


    @OnClick(R.id.fragment_dashboard_ll_ecrire_container)
    public void onEcrireContainerClicked() {
        mListener.writeUsClicked();
    }


    @OnClick(R.id.fragment_dashboard_ll_params_container)
    public void onLlParamsContainerClicked() {
        mListener.configClicked();
    }


    @OnClick(R.id.fragment_dashboard_ll_propos_container)
    public void onLlProposContainerClicked() {
        mListener.aboutClicked();
    }


    public interface OnDashboardFragmentListener {
        void showDashboardFragment();
        void profilClicked();
        void carnetClicked();
        void smsUserClicked();
        void reaboClicked();
        void myFamilyClicked();
        void writeUsClicked();
        void configClicked();
        void aboutClicked();
    }
}
