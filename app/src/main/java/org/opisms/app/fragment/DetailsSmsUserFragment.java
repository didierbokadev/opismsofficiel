package org.opisms.app.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.opisms.app.R;
import org.opisms.app.model.SmsUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


@SuppressWarnings("ALL")
public class DetailsSmsUserFragment extends Fragment {


    @BindView(R.id.fragment_details_sms_user_iv_home)
    AppCompatImageView ivHome;
    Unbinder unbinder;
    @BindView(R.id.fragment_details_sms_user_tv_sms_date)
    AppCompatTextView tvSmsDate;
    @BindView(R.id.fragment_details_sms_user_tv_sms_body)
    AppCompatTextView tvSmsBody;
    private OnDetailsSmsUserFragmentListener mListener;
    private SmsUser smsUser;


    public DetailsSmsUserFragment() {
        // Required empty public constructor
    }


    public static DetailsSmsUserFragment newInstance(SmsUser pSmsUser) {
        DetailsSmsUserFragment fragment = new DetailsSmsUserFragment();
        Bundle args = new Bundle();
        args.putParcelable("smsUserDetails", pSmsUser);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            smsUser = getArguments().getParcelable("smsUserDetails");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details_sms_user, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tvSmsBody.setText(smsUser.getSmsBody());
        tvSmsDate.setText(DateTime.parse(smsUser.getSmsDateEnvoi(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toString("dd/MM/yyyy 'à' HH:mm:ss" ));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDetailsSmsUserFragmentListener) {
            mListener = (OnDetailsSmsUserFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnDetailsSmsUserFragmentListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fragment_details_sms_user_iv_home)
    public void onBackClicked() {
        mListener.hideDetailsSmsUserFragment();
    }


    public interface OnDetailsSmsUserFragmentListener {
        void hideDetailsSmsUserFragment();
    }
}
