package org.opisms.app.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.opisms.app.R;
import org.opisms.app.adapter.FamilyAdapter;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.Family;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


@SuppressWarnings("ALL")
public class FamiliesListFragment extends Fragment {


    @BindView(R.id.fragment_families_list_iv_home)
    AppCompatImageView ivHome;
    @BindView(R.id.fragment_families_list_rv_families_list)
    RecyclerView rvFamiliesList;
    Unbinder unbinder;
    private OnFamiliesListFragmentListener mListener;
    List<Family> familyList;
    FamilyAdapter familyAdapter;
    OpiSmsDatabase opiSmsDatabase;


    public FamiliesListFragment() {
    }


    public static FamiliesListFragment newInstance() {
        FamiliesListFragment fragment = new FamiliesListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_families_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFamiliesListFragmentListener) {
            mListener = (OnFamiliesListFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFamiliesListFragmentListener");
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        opiSmsDatabase = OpiSmsDatabase.getDatabase(getActivity());

        familyList = new ArrayList<>();
        familyAdapter = new FamilyAdapter(getActivity(), familyList);

        rvFamiliesList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvFamiliesList.setAdapter(familyAdapter);

        new FetchFamiliesLocal().execute();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fragment_families_list_iv_home)
    public void onHomeClicked() {
        mListener.hideFamiliesListFragment();
    }


    private class FetchFamiliesLocal extends AsyncTask<Void, Void, List<Family>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mListener.fetchingFamiliesLoading();
        }

        @Override
        protected List<Family> doInBackground(Void... voids) {
            familyList.clear();
            familyList.addAll(opiSmsDatabase.familyDao().getFamilies());
            return familyList;
        }

        @Override
        protected void onPostExecute(List<Family> families) {
            super.onPostExecute(families);

            familyAdapter.notifyDataSetChanged();
            mListener.fetchingFamiliesFinished();
        }
    }


    public interface OnFamiliesListFragmentListener {
        void hideFamiliesListFragment();
        void fetchingFamiliesLoading();
        void fetchingFamiliesFinished();
    }
}
