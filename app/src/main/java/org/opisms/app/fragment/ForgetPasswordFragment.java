package org.opisms.app.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.opisms.app.R;
import org.opisms.app.model.Patient;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.UrlBanks;

import java.lang.reflect.Type;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@SuppressWarnings("ALL")
public class ForgetPasswordFragment extends Fragment {


    private OnForgetPasswordFragmentListener mListener;
    private AppCompatImageView imgBack;
    private AppCompatButton clickReset;
    private AppCompatEditText inputEmail;


    public ForgetPasswordFragment() {
    }


    public static ForgetPasswordFragment newInstance(String param1, String param2) {
        ForgetPasswordFragment fragment = new ForgetPasswordFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forget_password, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnForgetPasswordFragmentListener) {
            mListener = (OnForgetPasswordFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnForgetPasswordFragmentListener");
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        clickReset = view.findViewById(R.id.click_reset_password);
        imgBack = view.findViewById(R.id.img_back);
        inputEmail = view.findViewById(R.id.input_email);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        imgBack.setOnClickListener(viewClicked -> {
            mListener.hideForgetPassword();
        });

        clickReset.setOnClickListener(v -> {
            if (inputEmail.getText().toString().trim().length() == 0 && !RegexUtils.isEmail(inputEmail.getText().toString().trim())) {
                ToastUtils.showShort("Renseignez correctement votre adresse e-mail ! ");
                return;
            }
            mListener.forgetResetLoading(inputEmail.getText().toString().trim());
        });
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnForgetPasswordFragmentListener {
        void forgetResetLoading(String email);
        void forgetResetLoginEnd();
        void hideForgetPassword();
    }
}
