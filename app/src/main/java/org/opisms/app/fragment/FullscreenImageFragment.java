package org.opisms.app.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import org.opisms.app.R;

import uk.co.senab.photoview.PhotoView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FullscreenImageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@SuppressWarnings("ALL")
public class FullscreenImageFragment extends Fragment {


    private String carnetImage;
    private AppCompatImageView imageBack;
    private PhotoView photoView;
    private OnFullscreenImageFragmentListener mListener;


    public FullscreenImageFragment() {
    }


    public static FullscreenImageFragment newInstance(String pImage) {
        FullscreenImageFragment fragment = new FullscreenImageFragment();
        Bundle args = new Bundle();
        args.putString("carnet_image", pImage);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            carnetImage = getArguments().getString("carnet_image");
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFullscreenImageFragmentListener) {
            mListener = (OnFullscreenImageFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFullscreenImageFragmentListener");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fullscreen_image, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        photoView = view.findViewById(R.id.img_fullscreen);
        imageBack = view.findViewById(R.id.img_back);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        imageBack.setOnClickListener(v -> {
            mListener.hideFullscreenImage();
        });

        Glide.with(getActivity())
            .asBitmap()
            .load(Base64.decode(carnetImage, Base64.DEFAULT))
            .into(photoView);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFullscreenImageFragmentListener {
        void hideFullscreenImage();
    }
}
