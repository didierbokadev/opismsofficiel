package org.opisms.app.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.opisms.app.R;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.Patient;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.UrlBanks;

import java.lang.reflect.Type;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@SuppressWarnings("ALL")
public class LoginFragment extends Fragment {


	OnLoginFragmentListener mListener;
	ProgressDialog loading;

	// Widgets
    AppCompatEditText etLogin;
    AppCompatEditText etPassword;
    AppCompatButton btnConnection;
    AppCompatButton btnPasswordMissed;
    AppCompatTextView labelNewAccount;

    // Datas
    String strLogin;
    String strPassword;

    // Engine
    Gson gsonLogin;
    Type typeLogin;
    OpiSmsDatabase opiSmsDatabase;

    // Network jobs
    LoginUserAsync loginUserAsync;


    public LoginFragment() {
	}
	
	
	public static LoginFragment newInstance() {
		LoginFragment fragment = new LoginFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_login, container, false);
	}


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            etLogin = view.findViewById(R.id.fragment_login_et_email);
            etPassword = view.findViewById(R.id.fragment_login_et_pass);

            btnConnection = view.findViewById(R.id.fragment_login_btn_login);
            btnPasswordMissed = view.findViewById(R.id.fragment_login_btn_pass_forget);
            labelNewAccount = view.findViewById(R.id.label_new_account);
            opiSmsDatabase = OpiSmsDatabase.getDatabase(getActivity());

            // labelNewAccount.setVisibility(View.GONE);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {
            mListener.onLoginShow();

            loading = new ProgressDialog(getActivity());
            loading.setMessage("Connexion en cours..");
            loading.setCancelable(false);

            btnConnection.setOnClickListener(new View.OnClickListener()  {
                @Override
                public void onClick(View v) {
                strLogin = "183100";
                strPassword = "2721";

                if (etLogin.getText().toString().trim().length() == 0) {
                    return;
                }

                if (etPassword.getText().toString().trim().length() == 0) {
                    return;
                }

                strLogin = etLogin.getText().toString().trim();
                strPassword = etPassword.getText().toString().trim();

                loginUserAsync = new LoginUserAsync(strLogin, strPassword);
                loginUserAsync.execute();
                }
            });

            labelNewAccount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  int error = 1 / 0;
                    mListener.registerAccount();
                }
            });

            btnPasswordMissed.setOnClickListener(v -> {
                mListener.forgetPasswordAccount();
            });
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
	
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof OnLoginFragmentListener) {
			mListener = (OnLoginFragmentListener) context;
		} else {
			throw new RuntimeException(context.toString() + " mus t implement OnLoginFragmentListener");
		}
	}
	
	
	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}
	
	
	public interface OnLoginFragmentListener {
        void loading();
		void onLoginShow();
		void userLogged();
		void unloading();
		void registerAccount();
		void forgetPasswordAccount();
	}
	
	
	private class LoginUserAsync extends AsyncTask<Void, Void, Integer> {

	    private String login;
	    private String password;

	    LoginUserAsync(String pLogin, String pPassword) {
            this.login = pLogin;
            this.password = pPassword;

            if (gsonLogin == null)
                gsonLogin = new GsonBuilder().serializeNulls().create();

            if (typeLogin == null)
                typeLogin = new TypeToken<Patient>(){}.getType();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mListener.loading();
        }

        @Override
		protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient httpLogin = new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody paramsLogin = new FormBody.Builder()
                        .add("login", login)
                        .add("password", password)
                        .build();

                Request requestLogin = new Request.Builder()
                        .url(UrlBanks.LOGIN_USER)
                        .post(paramsLogin)
                        .build();

                Response responseLogin = httpLogin.newCall(requestLogin).execute();
                String responseStr = responseLogin.body().string();

                if (responseLogin.isSuccessful()) {
                    JSONObject jsonObjectLogin = new JSONObject(responseStr);

                    if (jsonObjectLogin.getInt("code") == 0) {
                        Patient patient = gsonLogin.fromJson(jsonObjectLogin.getJSONObject("data").toString(), typeLogin);

                        SPUtils.getInstance(Constants.USER_PREFS).put(Constants.USER_ID, patient.getPatId());
                        opiSmsDatabase.patientDao().createPatient(patient);
                    } else {
                        code = jsonObjectLogin.getInt("code");
                    }
                } else {
                    code = 4;
                }
            } catch (UnknownHostException uhex) {
                code = 4;
                uhex.printStackTrace();
            } catch (Exception ex) {
                code = 4;
                throw new RuntimeException(ex);
            }

			return code;
		}

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            mListener.unloading();

            switch (integer) {
                case 0 :
                    mListener.userLogged();
                break;

                case 1 :
                case 2 :
                    ToastUtils.showShort("Client inconnu !");
                break;
                default:
                    ToastUtils.showLong("Veuillez vous connecter à internet svp");
            }
        }
    }
}
