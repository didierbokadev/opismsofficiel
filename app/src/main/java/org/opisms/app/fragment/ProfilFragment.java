package org.opisms.app.fragment;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.opisms.app.AppController;
import org.opisms.app.R;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.Family;
import org.opisms.app.model.Patient;
import org.opisms.app.model.Vaccine;
import org.opisms.app.utils.Constants;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressWarnings("ALL")
public class ProfilFragment extends Fragment {


    @BindView(R.id.fragment_profil_iv_home)
    AppCompatImageView ivHome;
    Unbinder unbinder;
    OnProfilFragmentListener listener;
    @BindView(R.id.fragment_profil_tv_numero_pat)
    AppCompatTextView tvNumeroPat;
    @BindView(R.id.fragment_profil_tv_nom_prenoms)
    AppCompatTextView tvNomPrenoms;
    @BindView(R.id.fragment_profil_tv_naissance)
    AppCompatTextView tvNaissance;
    @BindView(R.id.fragment_profil_iv_carnet)
    AppCompatImageView ivPhoto;
    @BindView(R.id.fragment_profil_tv_sexe)
    AppCompatTextView tvSexe;
    @BindView(R.id.fragment_profil_tv_contact)
    AppCompatTextView tvContact;
    @BindView(R.id.fragment_profil_tv_date_abonnement)
    AppCompatTextView tvDateAbonnement;
    @BindView(R.id.fragment_profil_tv_date_expiration)
    AppCompatTextView tvDateExpiration;
    @BindView(R.id.fragment_profil_tv_assure)
    AppCompatTextView tvAssure;
    boolean gIsFamily;
    Patient patientInfos;
    Family gFamilyInfos;
    Gson gsonVacines;
    Type typeVacines;


    public ProfilFragment() {
    }


    public static ProfilFragment newInstance() {
        ProfilFragment fragment = new ProfilFragment();
        Bundle bundle = new Bundle();
        return fragment;
    }


    public static ProfilFragment newInstance(Family pFamily, boolean pIsFamily) {
        ProfilFragment fragment = new ProfilFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("is_family", pIsFamily);
        bundle.putParcelable("family_infos", pFamily);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gsonVacines = new GsonBuilder().serializeNulls().create();
        typeVacines = new TypeToken<ArrayList<Vaccine>>() {}.getType();

        if (getArguments() != null) {
            final Bundle lBundle = getArguments();

            gIsFamily = lBundle.getBoolean("is_family");
            gFamilyInfos = lBundle.getParcelable("family_infos");
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DashboardFragment.OnDashboardFragmentListener) {
            listener = (OnProfilFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnProfilFragmentListener");
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profil, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        patientInfos = OpiSmsDatabase.getDatabase(AppController.getContext()).patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"));

        if (gIsFamily) {
            patientInfos.setAbonneId(gFamilyInfos.getPatientId());
            patientInfos.setPatDate(gFamilyInfos.getPatientNaissance());
            patientInfos.setPatNom(gFamilyInfos.getNomPatient());
            patientInfos.setPatPrenoms(gFamilyInfos.getPrenomPatient());
            patientInfos.setPatSexe(gFamilyInfos.getPatientSexe());
            //  patientInfos.setPatDateAbonnement(gFamilyInfos.getPatientAbonnement());
            patientInfos.setPatFormuleId("INDEFINI");
            //  patientInfos.setPatDateExpiration(gFamilyInfos.getPatientExpiration());

            LogUtils.e("On here -> " + gFamilyInfos);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tvNumeroPat.setText(patientInfos.getPatNumero());
        tvNomPrenoms.setText(patientInfos.getPatNom() + " " + patientInfos.getPatPrenoms());
        tvNaissance.setText("Né(e) le " + patientInfos.getPatDate());
        tvSexe.setText(patientInfos.getPatSexe().equals("M") ? "Masculin" : "Feminin");
        tvContact.setText(patientInfos.getPatNumero());
        tvDateAbonnement.setText(patientInfos.getPatDateAbonnement());
        tvDateExpiration.setText(patientInfos.getPatDateExpiration());
        tvAssure.setText((patientInfos.getPatFormuleId().equals("3") || patientInfos.getPatFormuleId().equals("10")) ? "BUSINESS" : "PREMIUM");

        if (!StringUtils.isEmpty(patientInfos.getPatPhotoCarnt())) {
            Glide.with(getActivity())
                    .asBitmap()
                    .load(Base64.decode(patientInfos.getPatPhotoCarnt(), Base64.DEFAULT))
                    .into(ivPhoto);

            ivPhoto.setOnClickListener(v -> {
                listener.gotoFullscreen(patientInfos.getPatPhotoCarnt());
            });
        } else {
            ivPhoto.setImageResource(R.drawable.badge);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fragment_profil_iv_home)
    public void onHomeClicked() {
        listener.hideProfilFragment();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


    public interface OnProfilFragmentListener {
        void showProfilFragment();
        void hideProfilFragment();
        void gotoFullscreen(String imageUrl);
    }
}
