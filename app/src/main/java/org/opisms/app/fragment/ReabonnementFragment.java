package org.opisms.app.fragment;

import static org.opisms.app.utils.Constants.API_KEY;
import static org.opisms.app.utils.Constants.BUSINESS;
import static org.opisms.app.utils.Constants.generateTokenPayment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.room.util.StringUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.cinetpay.androidsdk.CinetPayActivity;
//  import com.cinetpay.sdk.MerchantService;
//  import com.cinetpay.sdk.PaymentResponse;
//  import com.cinetpay.sdk.Purchase;
//  import com.cinetpay.sdk.ServiceAccount;
//  import com.cinetpay.sdk.process.CinetPay;
//  import com.cinetpay.sdk.ui.CinetPayUI;

import org.opisms.app.AppController;
import org.opisms.app.R;
import org.opisms.app.activity.ReabonnementRecapActivity;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.CinetPayResponseModel;
import org.opisms.app.model.Patient;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.UrlBanks;

import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@SuppressWarnings("ALL")
public class ReabonnementFragment extends Fragment {


    @BindView(R.id.fragment_reabo_select_year)
    AppCompatSpinner selectReaboYear;
    @BindView(R.id.fragment_reabo_sv_infos_container)
    ScrollView scrollviewContainer;
    @BindView(R.id.fragment_reabo_ll_paiemntstatus_container)
    LinearLayout linearPaymentStatusContainer;
    @BindView(R.id.fragment_reabo_iv_paymentstatus_done)
    LottieAnimationView lottieStatus;
    @BindView(R.id.fragment_reabo_iv_paymentstatus_failed)
    AppCompatImageView ivPaymentStatusFailed;
    @BindView(R.id.fragment_vaccine_right_iv_home)
    AppCompatImageView ivBack;
    @BindView(R.id.fragment_reabonnement_ll_container)
    LinearLayout llReabnt;
    @BindView(R.id.fragment_reabo_et_name)
    AppCompatEditText etReaboName;
    @BindView(R.id.fragment_reabo_et_fullname)
    AppCompatEditText etReaboFullname;
    @BindView(R.id.fragment_reabo_et_amount)
    AppCompatEditText etReaboAmount;
    @BindView(R.id.fragment_reabo_et_formule)
    AppCompatEditText etReaboFormule;
    @BindView(R.id.fragment_reabo_tv_paymentstatus_label)
    AppCompatTextView tvPaymentStatus;
    @BindView(R.id.fragment_reabo_et_numero)
    AppCompatEditText etReaboNumero;
    int amountTopPay = 0;
    int validityAbonnement = 1;
    String methodPay = "MOBILE_MONEY";
    Unbinder unbinder;
    private OnReabonnementFragmentListener mListener;
    CinetPayResponseModel cinetResponse;

    OpiSmsDatabase opiSmsDatabase;
    Patient patient;


    public ReabonnementFragment() {
    }


    public static ReabonnementFragment newInstance(String param1, String param2) {
        ReabonnementFragment fragment = new ReabonnementFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);


        return fragment;
    }


    public static ReabonnementFragment newInstance() {
        ReabonnementFragment fragment = new ReabonnementFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reabonnement, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        opiSmsDatabase = OpiSmsDatabase.getDatabase(getActivity());

        patient = opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"));

        //purchase = new Purchase();
        // purchase.setCustomer()

        etReaboName.setText(patient.getPatNom());
        etReaboFullname.setText(patient.getPatPrenoms());

        etReaboFormule.setText((patient.getPatFormuleId().equals("3") || patient.getPatFormuleId().equals("10")) ? "BUSINESS" : "PREMIUM");
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReabonnementFragmentListener) {
            mListener = (OnReabonnementFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnReabonnementFragmentListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fragment_vaccine_right_iv_home)
    public void onBackClicked() {
        SPUtils.getInstance().remove("from");
        SPUtils.getInstance().remove("paiement");
        mListener.hideReabonnementFragment();
    }


    @OnClick({R.id.fragment_reabo_choice_mobile, R.id.fragment_reabo_choice_card})
    public void onRadioButtonClicked(RadioButton radioButton) {
        // Is the button now checked?
        boolean checked = radioButton.isChecked();

        // Check which radio button was clicked
        switch (radioButton.getId()) {
            case R.id.fragment_reabo_choice_mobile:
                if (checked) {
                    methodPay = "MOBILE_MONEY";
                }
                break;
            case R.id.fragment_reabo_choice_card:
                if (checked) {
                    methodPay = "CREDIT_CARD";
                }
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        LogUtils.e(SPUtils.getInstance().getString("paiement") == null ? " vide" : "info");
        LogUtils.e(SPUtils.getInstance().getString("paiement", "vide"));

        if (SPUtils.getInstance().getString("paiement", "vide") != "vide") {
            cinetResponse = GsonUtils.fromJson(SPUtils.getInstance().getString("paiement") , CinetPayResponseModel.class);
            scrollviewContainer.setVisibility(View.GONE);
            linearPaymentStatusContainer.setVisibility(View.VISIBLE);

            if (cinetResponse.getStatus().trim().equals("ACCEPTED")) {
                tvPaymentStatus.setText("Paiement effectué avec succes !");
                ivPaymentStatusFailed.setVisibility(View.GONE);
                lottieStatus.setVisibility(View.VISIBLE);
                lottieStatus.playAnimation();
                LogUtils.e("onResume " + cinetResponse.getStatus());

                new UpdateFormuleAsync().execute();
            } else {
                tvPaymentStatus.setText("Echec de paiement !");
                lottieStatus.setVisibility(View.GONE);
                ivPaymentStatusFailed.setVisibility(View.VISIBLE);
                LogUtils.e("onResume " + cinetResponse.getStatus());
            }
        } else {
            scrollviewContainer.setVisibility(View.VISIBLE);
            linearPaymentStatusContainer.setVisibility(View.GONE);
        }

        SPUtils.getInstance().remove("paiement");
    }


    @OnClick(R.id.fragment_reabonnement_ll_container)
    public void onReabonnementClicked() {
        Intent intent = new Intent(getActivity(), ReabonnementRecapActivity.class);

        intent.putExtra(CinetPayActivity.KEY_API_KEY, API_KEY);
        intent.putExtra(CinetPayActivity.KEY_SITE_ID, BUSINESS);
        intent.putExtra(CinetPayActivity.KEY_TRANSACTION_ID, generateTokenPayment());
        intent.putExtra(CinetPayActivity.KEY_AMOUNT, Integer.valueOf(amountTopPay));
        intent.putExtra(CinetPayActivity.KEY_CURRENCY, "XOF");
        intent.putExtra(CinetPayActivity.KEY_DESCRIPTION, "Reabonnement de formule");
        intent.putExtra(CinetPayActivity.KEY_CHANNELS, methodPay);
        intent.putExtra(CinetPayActivity.KEY_CUSTOMER_NAME, etReaboName.getText().toString().trim());
        intent.putExtra(CinetPayActivity.KEY_CUSTOMER_SURNAME, etReaboFullname.getText().toString().trim());
        intent.putExtra(CinetPayActivity.KEY_CUSTOMER_PHONE_NUMBER, etReaboNumero.getText().toString().trim());

        if (methodPay.contains("CREDIT_CARD")) {
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_NAME, patient.getPatNom().toUpperCase());
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_SURNAME, patient.getPatPrenoms().toUpperCase());
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_EMAIL, patient.getPatMail());
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_ADDRESS, "address");
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_PHONE_NUMBER, patient.getPatNumero());
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_CITY, "Abidjan");
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_COUNTRY, "CI");
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_ZIP_CODE, "CIV");
        }

        SPUtils.getInstance().put("from", "bills");
        intent.putExtra(CinetPayActivity.KEY_CHANNELS, methodPay);
        startActivity(intent);
    }


    @OnItemSelected(value = R.id.fragment_reabo_select_year, callback = OnItemSelected.Callback.ITEM_SELECTED)
    public void onSelectYear(int position) {
        validityAbonnement = position + 1;
        amountTopPay = 5500 * validityAbonnement;
        etReaboAmount.setText( amountTopPay + " Fcfa");
    }


    class UpdateFormuleAsync extends AsyncTask<Void, Void, Void> {

        ProgressDialog loading;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            patient.setPatValidite(String.valueOf(validityAbonnement));

            loading = new ProgressDialog(getActivity());
            loading.setMessage("Mise à jour des données...");
            loading.setCancelable(false);
            loading.setCanceledOnTouchOutside(false);
            loading.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                LogUtils.e("doInBackground: " + GsonUtils.toJson(patient));

                OkHttpClient formuleClient = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(0, TimeUnit.SECONDS)
                        .writeTimeout(0, TimeUnit.SECONDS)
                        .build();

                RequestBody formuleBody = new FormBody.Builder()
                        .add("patient", GsonUtils.toJson(patient))
                        .build();

                Request formuleRequest = new Request.Builder()
                        .method("POST", formuleBody)
                        .url(UrlBanks.USER_UPDATE_FORMULE)
                        .build();

                Response formulrResponse = formuleClient.newCall(formuleRequest).execute();
                String formuleString = formulrResponse.body().string();

                LogUtils.e(formuleString);
                OpiSmsDatabase.getDatabase(getActivity()).patientDao().createPatient(patient);
            } catch (Exception ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            loading.dismiss();
            //mListener.hideReabonnementFragment();
        }
    }


    public interface OnReabonnementFragmentListener {
        void hideReabonnementFragment();
    }
}
