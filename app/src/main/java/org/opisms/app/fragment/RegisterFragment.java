package org.opisms.app.fragment;

import static android.app.Activity.RESULT_OK;

import static org.opisms.app.utils.Constants.API_KEY;
import static org.opisms.app.utils.Constants.BUSINESS;
import static org.opisms.app.utils.Constants.generateTokenPayment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.core.content.FileProvider;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;

import com.airbnb.lottie.LottieAnimationView;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.cinetpay.androidsdk.CinetPayActivity;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ThreadUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.opisms.app.R;
import org.opisms.app.activity.PrivacyActivity;
import org.opisms.app.activity.ReabonnementRecapActivity;
import org.opisms.app.model.CinetPayResponseModel;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.ImageFilePath;
import org.opisms.app.utils.UrlBanks;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.RandomAccess;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.Unbinder;
import id.zelory.compressor.Compressor;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@SuppressWarnings("ALL")
public class RegisterFragment extends Fragment {


    private OnRegisterFragmentListener mListener;

    Unbinder unbinder;
    String carnetPhotoPath;
    File fileGlobal;
    int photoMethod = 1;

    @BindView(R.id.scroll_container)
    ScrollView scrollviewContainer;

    @BindView(R.id.fragment_register_ll_paiementstatus_container)
    LinearLayout paiementStatusContainer;

    @BindView(R.id.fragment_register_tv_paymentstatus_label)
    AppCompatTextView paiementStatusLabel;

    boolean privacyFlag = false;

    @BindView(R.id.fragment_register_iv_paymentstatus_done)
    LottieAnimationView lottiePaiementStatusDone;

    @BindView(R.id.fragment_register_iv_paymentstatus_failed)
    AppCompatImageView imagePaiementStatusFailed;

    @BindView(R.id.input_nom)
    AppCompatEditText etNom;
    @BindView(R.id.input_prenoms)
    AppCompatEditText etPrenoms;
    @BindView(R.id.fragment_register_et_amount)
    AppCompatEditText etAbonnementAmount;
    @BindView(R.id.input_email)
    AppCompatEditText etEmail;
    @BindView(R.id.input_telephone)
    AppCompatEditText etTelephone;
    @BindView(R.id.input_birthday)
    AppCompatEditText etBirthdate;
    @BindView(R.id.image_back)
    AppCompatImageView imageBack;
    @BindView(R.id.image_profil)
    AppCompatImageView imageProfilCarnet;
    @BindView(R.id.click_register)
    AppCompatButton clickRegistration;
    String transactionID = "";
    String birthdateRegExp = "^[0-31]{2}[0-12]{2}[1-9]{4}$";
    Pattern regexPattern = Pattern.compile(birthdateRegExp);
    Matcher matcher;
    private int validityAbonnement = 1;
    private String methodPay = "MOBILE_MONEY";
    private String abonnementType = "1";
    private String sexe = "M";
    private int amountTopPay = 5_500;

    CinetPayResponseModel cinetResponse;


    public RegisterFragment() {
    }


    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageBack = view.findViewById(R.id.image_back);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.e("" + carnetPhotoPath);

        if (data != null) LogUtils.e("Not null: " + data.getDataString()); else LogUtils.e("Null");

        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (photoMethod == 1) {
                setPic();
            } else {
                try {
                    Bundle extras = data.getExtras();
                    setPic(data.getData());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }


    @OnClick(R.id.image_back)
    public void onClickHome() {
        mListener.hideRegisterFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRegisterFragmentListener) {
            mListener = (OnRegisterFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnRegisterFragmentListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnCheckedChanged(R.id.fragment_register_check_privacy)
    public void onCheckPrivacy(boolean checkStatus) {
        privacyFlag = checkStatus;

        LogUtils.e("Status => " + privacyFlag);
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File carnetFile = null;
            try {
                carnetFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (carnetFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(), "org.opisms.app.provider", carnetFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        carnetPhotoPath = image.getAbsolutePath();
        return image;
    }


    public interface OnRegisterFragmentListener {
        void hideRegisterFragment();
        void userRegistered();
    }


    private void setPic() {
        // Get the dimensions of the View
        int targetW = imageProfilCarnet.getWidth();
        int targetH = imageProfilCarnet.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        imageProfilCarnet.setPadding(5, 5, 5, 5);
        imageProfilCarnet.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    private void setPic(Uri uriTraitment) {
        // Get the dimensions of the View
        int targetW = imageBack.getWidth();
        int targetH = imageBack.getHeight();


        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        carnetPhotoPath = ImageFilePath.getPath(getActivity(), uriTraitment);

        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoTakingW = bmOptions.outWidth;
        int photoTakingH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleTakingFactor = Math.min(photoTakingW/targetW, photoTakingH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleTakingFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        imageBack.setPadding(5, 5, 5, 5);
        imageBack.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    private void createImageFileCompressed() {
        File compressedFile = null;
        try {
            fileGlobal = new File(carnetPhotoPath);
            compressedFile = new Compressor(getActivity())
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(fileGlobal);
            if (FileUtils.isFileExists(compressedFile)) {
                final File finalCompressedFile = compressedFile;

                FileUtils.copyFile(compressedFile, fileGlobal, new FileUtils.OnReplaceListener() {
                    @Override
                    public boolean onReplace() {
                        return false;
                    }
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String encodeFileToBase64Binary(File file) {
        String encodedBase64 = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedBase64;
    }


    private void showFileChooser(int pView) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");

        if (intent.resolveActivity(getActivity().getPackageManager()) != null) { // Todo fix issue when pick photo into galerie via Fragment
            startActivityForResult(Intent.createChooser(intent, "Choisir une photo"), pView);
        }
    }


    @OnClick(R.id.fragment_register_iv_take_photo)
    public void onTakePhoto() {
        photoMethod = 1;
        dispatchTakePictureIntent();
    }


    @OnClick(R.id.fragment_register_iv_inert_photo)
    public void onPickImage() {
        photoMethod = 2;
        showFileChooser(1);
    }


    @OnClick({R.id.fragment_register_choice_femme, R.id.fragment_register_choice_homme})
    public void onRadioSexeButtonClicked(RadioButton radioButton) {
        // Is the button now checked?
        boolean checked = radioButton.isChecked();

        // Check which radio button was clicked
        switch (radioButton.getId()) {
            case R.id.fragment_register_choice_homme:
                if (checked) {
                    sexe = "M";
                }
                break;
            case R.id.fragment_register_choice_femme:
                if (checked) {
                    sexe = "F";
                }
                break;
        }
    }


    @OnClick({R.id.fragment_register_choice_mobile, R.id.fragment_register_choice_card})
    public void onRadioButtonClicked(RadioButton radioButton) {
        // Is the button now checked?
        boolean checked = radioButton.isChecked();

        // Check which radio button was clicked
        switch (radioButton.getId()) {
            case R.id.fragment_register_choice_mobile:
                if (checked) {
                    methodPay = "MOBILE_MONEY";
                }
                break;
            case R.id.fragment_register_choice_card:
                if (checked) {
                    methodPay = "CREDIT_CARD";
                }
                break;
        }
    }


    @OnClick({R.id.fragment_register_choice_vaccine, R.id.fragment_register_choice_planing})
    public void onAbonnementTypeClicked(RadioButton radioButton) {
        // Is the button now checked?
        boolean checked = radioButton.isChecked();

        // Check which radio button was clicked
        switch (radioButton.getId()) {
            case R.id.fragment_register_choice_vaccine:
                if (checked) {
                    abonnementType = "1";
                }
                break;
            case R.id.fragment_register_choice_planing:
                if (checked) {
                    abonnementType = "2";
                }
                break;
        }
    }


    @OnItemSelected(value = R.id.fragment_register_select_year, callback = OnItemSelected.Callback.ITEM_SELECTED)
    public void onSelectYear(int position) {
        validityAbonnement = position + 1;
        amountTopPay = 5_500 * validityAbonnement;
        etAbonnementAmount.setText( amountTopPay + " Fcfa");
    }


    @Override
    public void onResume() {
        super.onResume();

        LogUtils.e(SPUtils.getInstance().getString("paiement") == null ? " vide" : "info");
        LogUtils.e(SPUtils.getInstance().getString("paiement", "vide"));

        if (SPUtils.getInstance().getString("paiement", "vide") != "vide") {
            cinetResponse = GsonUtils.fromJson(SPUtils.getInstance().getString("paiement") , CinetPayResponseModel.class);
            scrollviewContainer.setVisibility(View.GONE);
            paiementStatusContainer.setVisibility(View.VISIBLE);

            if (cinetResponse.getStatus().trim().equals("ACCEPTED")) {
                paiementStatusLabel.setText("Paiement effectué avec succes !");
                imagePaiementStatusFailed.setVisibility(View.GONE);
                clickRegistration.setVisibility(View.GONE);
                lottiePaiementStatusDone.setVisibility(View.VISIBLE);
                lottiePaiementStatusDone.playAnimation();

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                LogUtils.e("onResume " + cinetResponse.getStatus());
                                new AbonnementEngine().execute();
                            }
                        },
                        3_000
                );
            } else {
                paiementStatusLabel.setText("Echec de paiement !");
                lottiePaiementStatusDone.setVisibility(View.GONE);
                imagePaiementStatusFailed.setVisibility(View.VISIBLE);
                clickRegistration.setText("Réessayez plus tard...");
                LogUtils.e("onResume " + cinetResponse.getStatus());
            }
        } else {
            scrollviewContainer.setVisibility(View.VISIBLE);
            paiementStatusContainer.setVisibility(View.GONE);
        }

        SPUtils.getInstance().remove("paiement");
    }


    @OnClick(value = R.id.fragment_register_tv_privacy)
    public void onPrivacyClicked() {
        ActivityUtils.startActivity(PrivacyActivity.class);
    }


    @OnClick(value = R.id.click_register)
    public void onRegisterClicked() {
        if (!privacyFlag) {
            ToastUtils.showLong("Veuillez lire et accepter les conditions d'utilisation, svp");
            return;
        }

        if (cinetResponse != null) {
            if (cinetResponse.getStatus().equals("REFUSED")) {
                mListener.hideRegisterFragment();
            }
        } else {
            if (etNom.getText().toString().trim().length() == 0
                    || etPrenoms.getText().toString().trim().length() == 0
                    || etEmail.getText().toString().trim().length() == 0
                    || etTelephone.getText().toString().trim().length() == 3
                    || etBirthdate.getText().toString().trim().length() == 0) {
                ToastUtils.showLong("Veuillez remplir les informations, svp");
                return;
            }

            transactionID = generateTokenPayment();

            Intent intent = new Intent(getActivity(), ReabonnementRecapActivity.class);
            intent.putExtra(CinetPayActivity.KEY_API_KEY, API_KEY);
            intent.putExtra(CinetPayActivity.KEY_SITE_ID, BUSINESS);
            intent.putExtra(CinetPayActivity.KEY_TRANSACTION_ID, transactionID);
            intent.putExtra(CinetPayActivity.KEY_AMOUNT, Integer.valueOf(amountTopPay));
            intent.putExtra(CinetPayActivity.KEY_CURRENCY, "XOF");
            intent.putExtra(CinetPayActivity.KEY_DESCRIPTION, "Abonnement au service de carnet electronique de vaccination");
            intent.putExtra(CinetPayActivity.KEY_CHANNELS, methodPay);
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_NAME, etNom.getText().toString().trim());
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_SURNAME, etPrenoms.getText().toString().trim());
            intent.putExtra(CinetPayActivity.KEY_CUSTOMER_PHONE_NUMBER, etTelephone.getText().toString().trim());
            intent.putExtra(CinetPayActivity.KEY_NOTIFY_URL, "https://www.opisms.net/opisms-ws/dev/api/v1/user/cinetpay/callback");

            if (methodPay.equals("CREDIT_CARD")) {
                intent.putExtra(CinetPayActivity.KEY_CUSTOMER_NAME,etNom.getText().toString().trim().toUpperCase());
                intent.putExtra(CinetPayActivity.KEY_CUSTOMER_SURNAME, etPrenoms.getText().toString().trim().toUpperCase());
                intent.putExtra(CinetPayActivity.KEY_CUSTOMER_EMAIL, etEmail.getText().toString().trim());
                intent.putExtra(CinetPayActivity.KEY_CUSTOMER_ADDRESS, "address");
                intent.putExtra(CinetPayActivity.KEY_CUSTOMER_PHONE_NUMBER, etTelephone.getText().toString().trim());
                intent.putExtra(CinetPayActivity.KEY_CUSTOMER_CITY, "Abidjan");
                intent.putExtra(CinetPayActivity.KEY_CUSTOMER_COUNTRY, "CI");
                intent.putExtra(CinetPayActivity.KEY_CUSTOMER_ZIP_CODE, "CIV");
            }

            SPUtils.getInstance().put("from", "bills");
            intent.putExtra(CinetPayActivity.KEY_CHANNELS, methodPay);
            startActivity(intent);
        }
    }


    class AbonnementEngine extends AsyncTask<String, Void, Void> {

        String nom;
        String prenoms;
        String dateNaissance;
        String email;
        String telephone;
        String nbreAnne;
        String ageGrossesse;
        String dateAccouchement;
        ProgressDialog loading;

        AbonnementEngine() {
            this.nom = etNom.getText().toString();
            this.prenoms = etPrenoms.getText().toString();
            this.dateNaissance = etBirthdate.getText().toString();
            this.telephone = etTelephone.getText().toString();
            this.email = etEmail.getText().toString().trim().isEmpty() ? "" : etEmail.getText().toString().trim();
            this.nbreAnne = String.valueOf(validityAbonnement);
            this.ageGrossesse = "";
            this.dateAccouchement = "";
        }

        @Override
        protected void onPreExecute() {
            lottiePaiementStatusDone.setVisibility(View.GONE);
            imagePaiementStatusFailed.setImageResource(0);
            imagePaiementStatusFailed.setVisibility(View.VISIBLE);
            paiementStatusLabel.setText("Finalisation de creation en cours...");

            Glide.with(getActivity())
                    .asGif()
                    .load(R.drawable.loading_spinner)
                    .into(imagePaiementStatusFailed);
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(0, TimeUnit.SECONDS)
                        .readTimeout(0, TimeUnit.SECONDS)
                        .build();

                // 18Z3579

                RequestBody formBody = new FormBody.Builder()
                        .add("ctrId", "25")
                        .add("usrId", "1")
                        .add("nPat", nom)
                        .add("pnPat", prenoms)
                        .add("ePat", email)
                        .add("dtPat", StringUtils.reverseDelimited(Constants.formatDate(dateNaissance), '-'))
                        .add("sxPat", sexe)
                        .add("lngPat", "FRANCAIS")
                        .add("tPat", telephone)
                        .add("abonType", abonnementType)
                        .add("dur", nbreAnne)
                        .add("preg", "N")
                        .add("recu", transactionID)
                        .add("agePreg", "")
                        .add("datePregEnd", "")
                        .add("photoCarnet", "")
                        .add("fm", "10")
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.REGISTER_USER)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();
                String reponse = clientResponse.body().string();
                LogUtils.e("Response: " + reponse);

                if (clientResponse.isSuccessful()) {

                    final JSONObject json = new JSONObject(reponse);
                    String statut = json.getString("statut");

                    if (statut.equals("1")) {
                        //final String patId = json.getString("patId");
                        //final String numRecu = json.getString("nuRecu");
                        final String patId = json.getString("idpat");
                        final String login = json.getString("login");
                        final String heure = DateTime.now().toString("HH:mm");
                        final String day = DateTime.now().toString("dd-MM-yyyy");
                        String dateExp = json.getString("dtExp");
                        DateTime date = new DateTime(dateExp);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mListener.userRegistered();
                                mListener.hideRegisterFragment();
                            }
                        });
                    } else {
                        String message = json.getString("message");
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                paiementStatusLabel.setText(message);
                                imagePaiementStatusFailed.setVisibility(View.GONE);
                                imagePaiementStatusFailed.setImageResource(R.drawable.cancel);
                                imagePaiementStatusFailed.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                } else {
                    LogUtils.e("Echec");
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            paiementStatusLabel.setText("Une erreur est survenue, réessayez plus tard");
                            imagePaiementStatusFailed.setVisibility(View.GONE);
                            imagePaiementStatusFailed.setImageResource(R.drawable.cancel);
                            imagePaiementStatusFailed.setVisibility(View.VISIBLE);
                        }
                    });
                }
            } catch (final Exception ex) {
                ex.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        paiementStatusLabel.setText("Une erreur est survenue, réessayez plus tard");
                        imagePaiementStatusFailed.setVisibility(View.GONE);
                        imagePaiementStatusFailed.setImageResource(R.drawable.cancel);
                        imagePaiementStatusFailed.setVisibility(View.VISIBLE);
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
        }
    }
}
