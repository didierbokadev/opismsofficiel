package org.opisms.app.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.opisms.app.AppController;
import org.opisms.app.R;
import org.opisms.app.adapter.SmsUserAdapter;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.SmsUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


@SuppressWarnings("ALL")
public class SmsUserFragment extends Fragment {


    @BindView(R.id.fragment_sms_user_iv_home)
    AppCompatImageView ivHome;
    @BindView(R.id.fragment_sms_user_rv_sms_list)
    RecyclerView rvSmsList;
    Unbinder unbinder;
    private OnSmsUserFragmentListener mListener;
    List<SmsUser> smsUserList;
    SmsUserAdapter smsUserAdapter;
    OpiSmsDatabase opiSmsDatabase;


    public SmsUserFragment() {
        // Required empty public constructor
    }


    public static SmsUserFragment newInstance() {
        SmsUserFragment fragment = new SmsUserFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sms_user, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        opiSmsDatabase = OpiSmsDatabase.getDatabase(AppController.getContext());

        smsUserList = new ArrayList<>();
        smsUserAdapter = new SmsUserAdapter(getActivity(), smsUserList);

        rvSmsList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvSmsList.setAdapter(smsUserAdapter);

        new FetchSmsUserLocal().execute();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSmsUserFragmentListener) {
            mListener = (OnSmsUserFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnSmsUserFragmentListener");
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @OnClick(R.id.fragment_sms_user_iv_home)
    public void onHomeClicked() {
       mListener.hideSmsUserFragment();
    }


    private class FetchSmsUserLocal extends AsyncTask<Void, Void, List<SmsUser>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mListener.smsUserLoading();
        }

        @Override
        protected List<SmsUser> doInBackground(Void... voids) {
            smsUserList.clear();
            smsUserList.addAll(opiSmsDatabase.smsUserDao().getSms());

            return smsUserList;
        }

        @Override
        protected void onPostExecute(List<SmsUser> smsUsers) {
            super.onPostExecute(smsUsers);

            if (smsUserList.size() == 0) {
                rvSmsList.setVisibility(View.GONE);
            } else {
                smsUserAdapter.notifyDataSetChanged();
            }

            mListener.smsUserUnloadng();
        }
    }


    public interface OnSmsUserFragmentListener {
        void hideSmsUserFragment();
        void smsUserLoading();
        void smsUserUnloadng();
    }
}
