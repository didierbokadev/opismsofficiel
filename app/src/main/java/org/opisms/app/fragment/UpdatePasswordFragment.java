package org.opisms.app.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;

import org.json.JSONObject;
import org.opisms.app.R;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.UrlBanks;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@SuppressWarnings("ALL")
public class UpdatePasswordFragment extends Fragment {


    @BindView(R.id.fragment_update_password_iv_home)
    AppCompatImageView ivBack;
    @BindView(R.id.fragment_update_password_et_old_pass)
    AppCompatEditText etPassOld;
    @BindView(R.id.fragment_update_password_et_new_pass)
    AppCompatEditText etPass;
    @BindView(R.id.fragment_update_password_et_confirm_pass)
    AppCompatEditText etPassOk;
    @BindView(R.id.fragment_update_password_btn_update_pass)
    AppCompatButton btnUpdate;
    Unbinder unbinder;
    private OnUpdatePasswordFragmentListener mListener;
    private String newPass;
    private String newPassConfirmed;
    private String oldPass;


    public UpdatePasswordFragment() {
        // Required empty public constructor
    }


    public static UpdatePasswordFragment newInstance(String param1, String param2) {
        UpdatePasswordFragment fragment = new UpdatePasswordFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public static UpdatePasswordFragment newInstance() {
        UpdatePasswordFragment fragment = new UpdatePasswordFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnUpdatePasswordFragmentListener) {
            mListener = (OnUpdatePasswordFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnUpdatePasswordFragmentListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fragment_update_password_iv_home)
    public void onIvBackClicked() {
        mListener.hideUpdatePasswordFragment();
    }


    @OnClick(R.id.fragment_update_password_btn_update_pass)
    public void onBtnUpdateClicked() {
        oldPass = etPassOld.getText().toString().trim();
        newPass = etPass.getText().toString().trim();
        newPassConfirmed = etPassOk.getText().toString().trim();

        if (oldPass.length() == 0 || newPass.length() == 0 || newPassConfirmed.length() == 0) {
            ToastUtils.showShort("Données incoherentes !");
            return;
        }

        if (!newPass.equals(newPassConfirmed)) {
            ToastUtils.showShort("Mots de passe differents !");
            return;
        }

        new UpdatePasswordAsync(newPass).execute();
    }


    public interface OnUpdatePasswordFragmentListener {
        void hideUpdatePasswordFragment();
        void startUpdatePass();
        void endUpdatePass(int code);
    }


    private class UpdatePasswordAsync extends AsyncTask<Void, Void, Integer> {

        String newPassAsync;

        UpdatePasswordAsync(String pNewPass) {
            this.newPassAsync = pNewPass;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mListener.startUpdatePass();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient passClient = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody passBody = new FormBody.Builder()
                        .add("userId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .add("userPass", newPassAsync)
                        .build();

                Request passRequest = new Request.Builder()
                        .url(UrlBanks.USER_UPDATE_PASSWORD)
                        .method("POST", passBody)
                        .build();

                Response passResponse = passClient.newCall(passRequest).execute();
                String passString = passResponse.body().string();

                JSONObject passJson = new JSONObject(passString);

                if (passJson.getInt("code") == 0) {
                    code = 0;
                } else {
                    code = 1;
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                code = 1;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            mListener.endUpdatePass(integer);

            if (integer == 0) {
                ToastUtils.showShort("Modification effectuée avec succes !");
            } else {
                ToastUtils.showShort("Une erreur est survenue !");
            }
        }
    }
}
