package org.opisms.app.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.opisms.app.R;
import org.opisms.app.adapter.VaccineAdapter;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.CalendrierVaccinal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


@SuppressWarnings("ALL")
public class VaccinesListFragment extends Fragment {


    @BindView(R.id.fragment_vaccines_list_iv_home)
    AppCompatImageView ivHome;
    @BindView(R.id.fragment_vaccines_list_rv_vaccines_list)
    RecyclerView rvVaccinesList;
    Unbinder unbinder;
    private OnVaccinesListFragmentListener mListener;
    List<CalendrierVaccinal> vaccinals;
    VaccineAdapter vaccineAdapter;
    OpiSmsDatabase opiSmsDatabase;


    public VaccinesListFragment() {
    }


    public static VaccinesListFragment newInstance() {
        VaccinesListFragment fragment = new VaccinesListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vaccines_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        opiSmsDatabase = OpiSmsDatabase.getDatabase(getActivity());

        vaccinals = new ArrayList<>();

        vaccineAdapter = new VaccineAdapter(getActivity(), vaccinals);

        rvVaccinesList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvVaccinesList.setAdapter(vaccineAdapter);

        new FetchVaccinesLocal().execute();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnVaccinesListFragmentListener) {
            mListener = (OnVaccinesListFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnVaccinesListFragmentListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fragment_vaccines_list_iv_home)
    public void onHomeClicked() {
        mListener.hideVaccinesListFragment();
    }


    public interface OnVaccinesListFragmentListener {
        void hideVaccinesListFragment();
        void fetchingVaccinesListLoading();
        void fetchingFinished();
    }


    private class FetchVaccinesLocal extends AsyncTask<Void, Void, List<CalendrierVaccinal>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mListener.fetchingVaccinesListLoading();
        }

        @Override
        protected List<CalendrierVaccinal> doInBackground(Void... voids) {
            vaccinals.clear();

            vaccinals.addAll(opiSmsDatabase.calendrierVaccinalDao().getCalendrierVaccinaux());

            return vaccinals;
        }

        @Override
        protected void onPostExecute(List<CalendrierVaccinal> calendrierVaccinals) {
            super.onPostExecute(calendrierVaccinals);

            vaccineAdapter.notifyDataSetChanged();

            mListener.fetchingFinished();
        }
    }
}
