package org.opisms.app.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.opisms.app.R;
import org.opisms.app.adapter.Vaccine2Adapter;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.Vaccine;
import org.opisms.app.utils.UrlBanks;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@SuppressWarnings("ALL")
public class
VaccinesMissedFragment extends Fragment {


    @BindView(R.id.fragment_vaccine_missed_iv_home)
    AppCompatImageView ivBack;
    @BindView(R.id.fragment_vaccine_missed_rv_list)
    RecyclerView rvList;
    @BindView(R.id.fragment_vaccine_missed_ll_no_vaccin)
    LinearLayout llNoVaccin;
    Unbinder unbinder;
    private OnVaccinesMissedFragmentListener mListener;
    OpiSmsDatabase database;
    List<Vaccine> vaccineList;
    Vaccine2Adapter vaccine2Adapter;
    boolean gCallRemote;
    String gPatientID;
    Gson gsonVacines;
    Type typeVacines;


    public VaccinesMissedFragment() {
        // Required empty public constructor
    }


    public static VaccinesMissedFragment newInstance(String param1, String param2) {
        VaccinesMissedFragment fragment = new VaccinesMissedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public static VaccinesMissedFragment newInstance() {
        VaccinesMissedFragment fragment = new VaccinesMissedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public static VaccinesMissedFragment newInstance(String pParentID, boolean pCallRemoteDatas) {
        VaccinesMissedFragment fragment = new VaccinesMissedFragment();
        Bundle args = new Bundle();
        args.putString("family_id", pParentID);
        args.putBoolean("call_remote", pCallRemoteDatas);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            final Bundle lBundle = getArguments();

            gPatientID = lBundle.getString("family_id", "1");
            gCallRemote = lBundle.getBoolean("call_remote");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vaccine_missed, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        gsonVacines = new GsonBuilder().serializeNulls().create();
        typeVacines = new TypeToken<ArrayList<Vaccine>>() {}.getType();

        database =  OpiSmsDatabase.getDatabase(getActivity());

        vaccineList = new ArrayList<>();
        vaccine2Adapter = new Vaccine2Adapter(getActivity(), vaccineList);

        rvList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvList.setAdapter(vaccine2Adapter);

        if (gCallRemote)
            new FetchVacinesMissedRemoteAsync().execute();
        else
            new FetchVacinesMissedAsync().execute();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnVaccinesMissedFragmentListener) {
            mListener = (OnVaccinesMissedFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnVaccinesMissedFragmentListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fragment_vaccine_missed_iv_home)
    public void onBack() {
        mListener.hideVaccinesMissedFragment();
    }


    public interface OnVaccinesMissedFragmentListener {
        void hideVaccinesMissedFragment();
        void startFetchVacinesRight();
        void endFetchVacinesRight();
    }


    class FetchVacinesMissedRemoteAsync extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mListener.startFetchVacinesRight();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient rightClient = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody rightForms = new FormBody.Builder()
                        .add("patId", gPatientID)
                        .build();

                Request rightRequest = new Request.Builder()
                        .url(UrlBanks.USER_VACCINE_MISSED_LIST)
                        .method("POST", rightForms)
                        .build();

                Response rightResponse = rightClient.newCall(rightRequest).execute();
                String rightString = rightResponse.body().string();

                LogUtils.e("TAG", rightString);

                if (rightResponse.isSuccessful()) {
                    JSONObject rightJson = new JSONObject(rightString);

                    if (rightJson.getInt("statut") == 1) {
                        code = 0;
                        vaccineList.clear();
                        vaccineList.addAll((ArrayList<Vaccine>) gsonVacines.fromJson(rightJson.getJSONArray("data").toString(), typeVacines));
                    } else {
                        code = 1;
                    }
                } else {
                    code = 2;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                code = 3;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            vaccine2Adapter.notifyDataSetChanged();
            mListener.endFetchVacinesRight();
        }
    }


    class FetchVacinesMissedAsync extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mListener.startFetchVacinesRight();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                vaccineList.clear();
                vaccineList.addAll(database.vaccineDao().getVaccinesByType("2"));

            } catch (Exception ex) {
                ex.printStackTrace();
                code = 1;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            if (vaccineList.size() == 0) {
                rvList.setVisibility(View.GONE);
                llNoVaccin.setVisibility(View.VISIBLE);
            } else {
                vaccine2Adapter.notifyDataSetChanged();
                rvList.setVisibility(View.VISIBLE);
                llNoVaccin.setVisibility(View.GONE);
            }

            mListener.endFetchVacinesRight();
        }
    }
}
