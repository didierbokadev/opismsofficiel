package org.opisms.app.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.JsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;

import org.opisms.app.R;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.UrlBanks;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@SuppressWarnings("ALL")
public class WriteUsFragment extends Fragment {


    @BindView(R.id.fragment_write_us_iv_home)
    AppCompatImageView ivHome;
    @BindView(R.id.fragment_write_us_et_objet_msg)
    AppCompatEditText etSubject;
    @BindView(R.id.fragment_write_us_et_message)
    AppCompatEditText etMessage;
    @BindView(R.id.fragment_write_us_btn_send)
    AppCompatButton btnSend;
    Unbinder unbinder;
    private OnWriteUsFragmentListener mListener;
    String subject;
    String message;
    OpiSmsDatabase opiSmsDatabase;
    ProgressDialog loading;


    public WriteUsFragment() {
        // Required empty public constructor
    }


    public static WriteUsFragment newInstance(String param1, String param2) {
        WriteUsFragment fragment = new WriteUsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public static WriteUsFragment newInstance() {
        WriteUsFragment fragment = new WriteUsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_write_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loading = new ProgressDialog(getActivity());
        loading.setMessage("Envoi du mail...");
        loading.setCancelable(false);

        opiSmsDatabase = OpiSmsDatabase.getDatabase(getActivity());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWriteUsFragmentListener) {
            mListener = (OnWriteUsFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnWriteUsFragmentListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fragment_write_us_iv_home)
    public void onIvHomeClicked() {
        mListener.hideWriteUsFragment();
    }


    @OnClick(R.id.fragment_write_us_btn_send)
    public void onSendClicked() {
        subject = etSubject.getText().toString().trim();
        message = etMessage.getText().toString().trim();

        if (StringUtils.isEmpty(subject)) {
            ToastUtils.showShort("Object vide !");
            return;
        }

        if (StringUtils.isEmpty(message)) {
            ToastUtils.showShort("Message vide !");
            return;
        }

        loading.show();
        new SendEmail().execute();
    }


    public interface OnWriteUsFragmentListener {
        void hideWriteUsFragment();
    }


    class SendEmail extends AsyncTask<Void, Void, Integer> {

        public SendEmail() {
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                OkHttpClient mailClient = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody mailBody = new FormBody.Builder()
                        .add("patient", GsonUtils.toJson(opiSmsDatabase.patientDao().getPatientById(SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))))
                        .add("object", subject)
                        .add("message", message)
                        .build();

                Request mailRequest = new Request.Builder()
                        .method("POST", mailBody)
                        .url(UrlBanks.USER_SEND_MAIL)
                        .build();

                Response mailResponse = mailClient.newCall(mailRequest).execute();
                String mailString = mailResponse.body().string();

                LogUtils.e("TAG", mailString);

                if (JsonUtils.getInt(mailString, "code") == 0) {
                    code = 0;
                } else {
                    code = 1;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                code = 1;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            loading.dismiss();

            if (integer == 0) {
                ToastUtils.showLong("Mail envoyé !");
                mListener.hideWriteUsFragment();
            } else {
                ToastUtils.showLong("Mail non envoyé !");
            }
        }
    }
}
