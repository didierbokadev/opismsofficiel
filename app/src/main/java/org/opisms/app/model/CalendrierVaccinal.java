package org.opisms.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Cedric.BOKA on 12/02/2019.
 */

@Entity(
        tableName = "calendrier_vaccinal",
        indices = @Index(
                value = "calendrier_id",
                unique = true
        )
)
@SuppressWarnings("ALL")
public class CalendrierVaccinal implements Parcelable {


    /**
     * IDCAL : 478517
     * NOMVAC : FIEVRE JAUNE
     * DATERAPEL : 2013-12-12
     * PRESENCE : 2013-12-12
     * LOVAC : 127VFC044Z
     * NOMCENTR : INHP-TREICH
     * IDPAT : 216
     */

    @Expose(deserialize = false, serialize = false)
    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    private long calendrierLocalId;

    @SerializedName("IDCAL")
    @ColumnInfo(name = "calendrier_id")
    private String calendrierId;

    @SerializedName("NOMVAC")
    @ColumnInfo(name = "vaccin_nom")
    private String vaccinNom;

    @SerializedName("DATERAPEL")
    @ColumnInfo(name = "date_rappel")
    private String dateRappel;

    @SerializedName("PRESENCE")
    @ColumnInfo(name = "date_presence")
    private String datePresence;

    @SerializedName("LOVAC")
    @ColumnInfo(name = "vaccin_lot")
    private String vaccinLot;

    @SerializedName("NOMCENTR")
    @ColumnInfo(name = "centre_nom")
    private String centreNom;

    @SerializedName("IDPAT")
    @ColumnInfo(name = "patient_id")
    private String patientId;


    public CalendrierVaccinal() {
    }


    public long getCalendrierLocalId() {
        return calendrierLocalId;
    }

    public void setCalendrierLocalId(long calendrierLocalId) {
        this.calendrierLocalId = calendrierLocalId;
    }

    public String getCalendrierId() {
        return calendrierId;
    }

    public void setCalendrierId(String calendrierId) {
        this.calendrierId = calendrierId;
    }

    public String getVaccinNom() {
        return vaccinNom;
    }

    public void setVaccinNom(String vaccinNom) {
        this.vaccinNom = vaccinNom;
    }

    public String getDateRappel() {
        return dateRappel;
    }

    public void setDateRappel(String dateRappel) {
        this.dateRappel = dateRappel;
    }

    public String getDatePresence() {
        return datePresence;
    }

    public void setDatePresence(String datePresence) {
        this.datePresence = datePresence;
    }

    public String getVaccinLot() {
        return vaccinLot;
    }

    public void setVaccinLot(String vaccinLot) {
        this.vaccinLot = vaccinLot;
    }

    public String getCentreNom() {
        return centreNom;
    }

    public void setCentreNom(String centreNom) {
        this.centreNom = centreNom;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.calendrierId);
        dest.writeString(this.vaccinNom);
        dest.writeString(this.dateRappel);
        dest.writeString(this.datePresence);
        dest.writeString(this.vaccinLot);
        dest.writeString(this.centreNom);
        dest.writeString(this.patientId);
    }

    protected CalendrierVaccinal(Parcel in) {
        this.calendrierId = in.readString();
        this.vaccinNom = in.readString();
        this.dateRappel = in.readString();
        this.datePresence = in.readString();
        this.vaccinLot = in.readString();
        this.centreNom = in.readString();
        this.patientId = in.readString();
    }

    public static final Parcelable.Creator<CalendrierVaccinal> CREATOR = new Parcelable.Creator<CalendrierVaccinal>() {
        @Override
        public CalendrierVaccinal createFromParcel(Parcel source) {
            return new CalendrierVaccinal(source);
        }

        @Override
        public CalendrierVaccinal[] newArray(int size) {
            return new CalendrierVaccinal[size];
        }
    };
}
