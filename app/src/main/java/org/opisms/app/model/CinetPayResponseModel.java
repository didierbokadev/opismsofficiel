package org.opisms.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Didier BOKA, email: didierboka.developer@gmail.com
 * on 07/06/2022.
 **/

public class CinetPayResponseModel implements Parcelable {


    @Expose
    @SerializedName("payment_date")
    private String paymentDate;
    @Expose
    @SerializedName("operator_id")
    private String operatorId;
    @Expose
    @SerializedName("metadata")
    private String metadata;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("payment_method")
    private String paymentMethod;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("amount")
    private String amount;


    public CinetPayResponseModel() {
    }


    public CinetPayResponseModel(String paymentDate, String operatorId, String metadata, String description, String paymentMethod, String status, String currency, String amount) {
        this.paymentDate = paymentDate;
        this.operatorId = operatorId;
        this.metadata = metadata;
        this.description = description;
        this.paymentMethod = paymentMethod;
        this.status = status;
        this.currency = currency;
        this.amount = amount;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.paymentDate);
        dest.writeString(this.operatorId);
        dest.writeString(this.metadata);
        dest.writeString(this.description);
        dest.writeString(this.paymentMethod);
        dest.writeString(this.status);
        dest.writeString(this.currency);
        dest.writeString(this.amount);
    }

    public void readFromParcel(Parcel source) {
        this.paymentDate = source.readString();
        this.operatorId = source.readString();
        this.metadata = source.readString();
        this.description = source.readString();
        this.paymentMethod = source.readString();
        this.status = source.readString();
        this.currency = source.readString();
        this.amount = source.readString();
    }

    protected CinetPayResponseModel(Parcel in) {
        this.paymentDate = in.readString();
        this.operatorId = in.readString();
        this.metadata = in.readString();
        this.description = in.readString();
        this.paymentMethod = in.readString();
        this.status = in.readString();
        this.currency = in.readString();
        this.amount = in.readString();
    }

    public static final Parcelable.Creator<CinetPayResponseModel> CREATOR = new Parcelable.Creator<CinetPayResponseModel>() {
        @Override
        public CinetPayResponseModel createFromParcel(Parcel source) {
            return new CinetPayResponseModel(source);
        }

        @Override
        public CinetPayResponseModel[] newArray(int size) {
            return new CinetPayResponseModel[size];
        }
    };
}
