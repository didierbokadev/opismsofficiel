package org.opisms.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Cedric.BOKA on 13/02/2019.
 */

@Entity(
        tableName = "family",
        indices = {
                @Index(
                        value = "patient_id"
                )
        }
)
@SuppressWarnings("ALL")
public class Family implements Parcelable {


    /**
     * IDPAT : 4843
     * NOMPAT : NDRIN
     * PRENOMPAT : ETCHE IVAN
     * SEXEPAT : M
     * DATEPAT : 2010-07-14
     * DATE ABONNEMENT : 20212-12-21
     * DATE EXPIRATION : 2021-12-21
     */


    @Expose(deserialize = false, serialize = false)
    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    private long patientLocalId;

    @SerializedName("IDPAT")
    @ColumnInfo(name = "patient_id")
    private String patientId;

    @SerializedName("NOMPAT")
    @ColumnInfo(name = "patient_nom")
    private String nomPatient;

    @SerializedName("PRENOMPAT")
    @ColumnInfo(name = "patient_prenoms")
    private String prenomPatient;

    @SerializedName("SEXEPAT")
    @ColumnInfo(name = "patient_sexe")
    private String patientSexe;

    @SerializedName("DATEPAT")
    @ColumnInfo(name = "patient_naissance")
    private String patientNaissance;

    @SerializedName("DATEABONNEMENT")
    @ColumnInfo(name = "patient_abonnement")
    private String patientAbonnement;

    @SerializedName("DATEEXPIRATION")
    @ColumnInfo(name = "patient_expiration")
    private String patientExpiration;


    public Family() {
    }

    public String getPatientAbonnement() {
        return patientAbonnement;
    }

    public void setPatientAbonnement(String patientAbonnement) {
        this.patientAbonnement = patientAbonnement;
    }

    public String getPatientExpiration() {
        return patientExpiration;
    }

    public void setPatientExpiration(String patientExpiration) {
        this.patientExpiration = patientExpiration;
    }

    public long getPatientLocalId() {
        return patientLocalId;
    }

    public void setPatientLocalId(long patientLocalId) {
        this.patientLocalId = patientLocalId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getNomPatient() {
        return nomPatient;
    }

    public void setNomPatient(String nomPatient) {
        this.nomPatient = nomPatient;
    }

    public String getPrenomPatient() {
        return prenomPatient;
    }

    public void setPrenomPatient(String prenomPatient) {
        this.prenomPatient = prenomPatient;
    }

    public String getPatientSexe() {
        return patientSexe;
    }

    public void setPatientSexe(String patientSexe) {
        this.patientSexe = patientSexe;
    }

    public String getPatientNaissance() {
        return patientNaissance;
    }

    public void setPatientNaissance(String patientNaissance) {
        this.patientNaissance = patientNaissance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.patientId);
        dest.writeString(this.nomPatient);
        dest.writeString(this.prenomPatient);
        dest.writeString(this.patientSexe);
        dest.writeString(this.patientNaissance);
        dest.writeString(this.patientAbonnement);
        dest.writeString(this.patientExpiration);
    }

    protected Family(Parcel in) {
        this.patientId = in.readString();
        this.nomPatient = in.readString();
        this.prenomPatient = in.readString();
        this.patientSexe = in.readString();
        this.patientNaissance = in.readString();
        this.patientAbonnement = in.readString();
        this.patientExpiration = in.readString();
    }



    public static final Parcelable.Creator<Family> CREATOR = new Parcelable.Creator<Family>() {
        @Override
        public Family createFromParcel(Parcel source) {
            return new Family(source);
        }

        @Override
        public Family[] newArray(int size) {
            return new Family[size];
        }
    };
}
