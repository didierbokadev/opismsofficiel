package org.opisms.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.blankj.utilcode.util.GsonUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Cedric.BOKA on 05/02/2019.
 */


@SuppressWarnings("ALL")
@Entity(
        tableName = "patient",
        indices = @Index(
                    value = "patient_id",
                    unique = true
                    )
)
public class Patient implements Parcelable {


    /**
     * ID_ABONN : 993839
     * IDPAT : 216
     * NOMPAT : NDRIN
     * PRENOMPAT : ETCHE NOEL
     * DATEPAT : 1971-12-24
     * NUMEROPAT : 22579434001
     * EMAILPAT : noeletc@yahoo.fr
     * ACTIVITE : INFORMATICIEN
     * PROVENANCE :
     * DATE_ABONN : 2019-02-05
     * NUMRECU : 19Z23496
     * VALIDITE : 1
     * DATESAISIE : 2010-07-01
     * LOGIN : 183100
     * SEXEPAT : M
     */

    @Expose(deserialize = false, serialize = false)
    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    private long abonneLocalId;

    @SerializedName("ID_ABONN")
    @ColumnInfo(name = "remote_id")
    private String abonneId;

    @SerializedName("IDPAT")
    @ColumnInfo(name = "patient_id")
    private String patId;

    @SerializedName("NOMPAT")
    @ColumnInfo(name = "patient_nom")
    private String patNom;

    @SerializedName("PRENOMPAT")
    @ColumnInfo(name = "patient_prenoms")
    private String patPrenoms;

    @SerializedName("DATEPAT")
    @ColumnInfo(name = "patient_date")
    private String patDate;

    @SerializedName("NUMEROPAT")
    @ColumnInfo(name = "patient_numero")
    private String patNumero;

    @SerializedName("EMAILPAT")
    @ColumnInfo(name = "patient_email")
    private String patMail;

    @SerializedName("PHOTOCARNET")
    @ColumnInfo(name = "patient_photo_carnet")
    private String patPhotoCarnt;

    @SerializedName("ACTIVITE")
    @ColumnInfo(name = "patient_activite")
    private String patActivite;

    @SerializedName("PROVENANCE")
    @ColumnInfo(name = "patient_provenance")
    private String patProvenance;

    @SerializedName("DATE_ABONN")
    @ColumnInfo(name = "patient_abonnement")
    private String patDateAbonnement;

    @SerializedName("DATE_EXPIRATION")
    @ColumnInfo(name = "patient_abonnement_exp")
    private String patDateExpiration;

    @SerializedName("NUMRECU")
    @ColumnInfo(name = "patient_recu")
    private String patNumRecu;

    @SerializedName("IDFORMULE")
    @ColumnInfo(name = "patient_formule")
    private String patFormuleId;

    @SerializedName("VALIDITE")
    @ColumnInfo(name = "patient_validite")
    private String patValidite;

    @SerializedName("DATESAISIE")
    @ColumnInfo(name = "patient_date_saisie")
    private String patDateSaisie;

    @SerializedName("LOGIN")
    @ColumnInfo(name = "patient_login")
    private String patLogin;

    @SerializedName("SEXEPAT")
    @ColumnInfo(name = "patient_sexe")
    private String patSexe;


    public Patient() {
    }



    public String getPatPhotoCarnt() {
        return patPhotoCarnt;
    }

    public void setPatPhotoCarnt(String patPhotoCarnt) {
        this.patPhotoCarnt = patPhotoCarnt;
    }

    public String getPatDateExpiration() {
        return patDateExpiration;
    }

    public void setPatDateExpiration(String patDateExpiration) {
        this.patDateExpiration = patDateExpiration;
    }

    public String getPatFormuleId() {
        return patFormuleId;
    }

    public void setPatFormuleId(String patFormuleId) {
        this.patFormuleId = patFormuleId;
    }

    public long getAbonneLocalId() {
        return abonneLocalId;
    }

    public void setAbonneLocalId(long abonneLocalId) {
        this.abonneLocalId = abonneLocalId;
    }

    public String getAbonneId() {
        return abonneId;
    }

    public void setAbonneId(String abonneId) {
        this.abonneId = abonneId;
    }

    public String getPatId() {
        return patId;
    }

    public void setPatId(String patId) {
        this.patId = patId;
    }

    public String getPatNom() {
        return patNom;
    }

    public void setPatNom(String patNom) {
        this.patNom = patNom;
    }

    public String getPatPrenoms() {
        return patPrenoms;
    }

    public void setPatPrenoms(String patPrenoms) {
        this.patPrenoms = patPrenoms;
    }

    public String getPatDate() {
        return patDate;
    }

    public void setPatDate(String patDate) {
        this.patDate = patDate;
    }

    public String getPatNumero() {
        return patNumero;
    }

    public void setPatNumero(String patNumero) {
        this.patNumero = patNumero;
    }

    public String getPatMail() {
        return patMail;
    }

    public void setPatMail(String patMail) {
        this.patMail = patMail;
    }

    public String getPatActivite() {
        return patActivite;
    }

    public void setPatActivite(String patActivite) {
        this.patActivite = patActivite;
    }

    public String getPatProvenance() {
        return patProvenance;
    }

    public void setPatProvenance(String patProvenance) {
        this.patProvenance = patProvenance;
    }

    public String getPatDateAbonnement() {
        return patDateAbonnement;
    }

    public void setPatDateAbonnement(String patDateAbonnement) {
        this.patDateAbonnement = patDateAbonnement;
    }

    public String getPatNumRecu() {
        return patNumRecu;
    }

    public void setPatNumRecu(String patNumRecu) {
        this.patNumRecu = patNumRecu;
    }

    public String getPatValidite() {
        return patValidite;
    }

    public void setPatValidite(String patValidite) {
        this.patValidite = patValidite;
    }

    public String getPatDateSaisie() {
        return patDateSaisie;
    }

    public void setPatDateSaisie(String patDateSaisie) {
        this.patDateSaisie = patDateSaisie;
    }

    public String getPatLogin() {
        return patLogin;
    }

    public void setPatLogin(String patLogin) {
        this.patLogin = patLogin;
    }

    public String getPatSexe() {
        return patSexe;
    }

    public void setPatSexe(String patSexe) {
        this.patSexe = patSexe;
    }


    @Override
    public String toString() {
        return GsonUtils.toJson(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.abonneLocalId);
        dest.writeString(this.abonneId);
        dest.writeString(this.patId);
        dest.writeString(this.patNom);
        dest.writeString(this.patPrenoms);
        dest.writeString(this.patDate);
        dest.writeString(this.patNumero);
        dest.writeString(this.patMail);
        dest.writeString(this.patPhotoCarnt);
        dest.writeString(this.patActivite);
        dest.writeString(this.patProvenance);
        dest.writeString(this.patDateAbonnement);
        dest.writeString(this.patDateExpiration);
        dest.writeString(this.patNumRecu);
        dest.writeString(this.patFormuleId);
        dest.writeString(this.patValidite);
        dest.writeString(this.patDateSaisie);
        dest.writeString(this.patLogin);
        dest.writeString(this.patSexe);
    }

    protected Patient(Parcel in) {
        this.abonneLocalId = in.readLong();
        this.abonneId = in.readString();
        this.patId = in.readString();
        this.patNom = in.readString();
        this.patPrenoms = in.readString();
        this.patDate = in.readString();
        this.patNumero = in.readString();
        this.patMail = in.readString();
        this.patPhotoCarnt = in.readString();
        this.patActivite = in.readString();
        this.patProvenance = in.readString();
        this.patDateAbonnement = in.readString();
        this.patDateExpiration = in.readString();
        this.patNumRecu = in.readString();
        this.patFormuleId = in.readString();
        this.patValidite = in.readString();
        this.patDateSaisie = in.readString();
        this.patLogin = in.readString();
        this.patSexe = in.readString();
    }

    public static final Creator<Patient> CREATOR = new Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel source) {
            return new Patient(source);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };
}
