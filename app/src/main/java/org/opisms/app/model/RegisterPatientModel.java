package org.opisms.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Didier BOKA, email: didierboka.developer@gmail.com
 * on 08/06/2022.
 **/

public class RegisterPatientModel {


    @Expose
    @SerializedName("datePregEnd")
    private String datepregend;
    @Expose
    @SerializedName("agePreg")
    private String agepreg;
    @Expose
    @SerializedName("preg")
    private String preg;
    @Expose
    @SerializedName("fm")
    private String fm;
    @Expose
    @SerializedName("recu")
    private String recu;
    @Expose
    @SerializedName("dur")
    private String dur;
    @Expose
    @SerializedName("sxPat")
    private String sxpat;
    @Expose
    @SerializedName("dtPat")
    private String dtpat;
    @Expose
    @SerializedName("usrId")
    private String usrid;
    @Expose
    @SerializedName("ctrId")
    private String ctrid;
    @Expose
    @SerializedName("photoCarnet")
    private String photocarnet;
    @Expose
    @SerializedName("tPat")
    private String tpat;
    @Expose
    @SerializedName("pnPat")
    private String pnpat;
    @Expose
    @SerializedName("nPat")
    private String npat;


    public RegisterPatientModel() {
    }


    public String getDatepregend() {
        return datepregend;
    }

    public void setDatepregend(String datepregend) {
        this.datepregend = datepregend;
    }

    public String getAgepreg() {
        return agepreg;
    }

    public void setAgepreg(String agepreg) {
        this.agepreg = agepreg;
    }

    public String getPreg() {
        return preg;
    }

    public void setPreg(String preg) {
        this.preg = preg;
    }

    public String getFm() {
        return fm;
    }

    public void setFm(String fm) {
        this.fm = fm;
    }

    public String getRecu() {
        return recu;
    }

    public void setRecu(String recu) {
        this.recu = recu;
    }

    public String getDur() {
        return dur;
    }

    public void setDur(String dur) {
        this.dur = dur;
    }

    public String getSxpat() {
        return sxpat;
    }

    public void setSxpat(String sxpat) {
        this.sxpat = sxpat;
    }

    public String getDtpat() {
        return dtpat;
    }

    public void setDtpat(String dtpat) {
        this.dtpat = dtpat;
    }

    public String getUsrid() {
        return usrid;
    }

    public void setUsrid(String usrid) {
        this.usrid = usrid;
    }

    public String getCtrid() {
        return ctrid;
    }

    public void setCtrid(String ctrid) {
        this.ctrid = ctrid;
    }

    public String getPhotocarnet() {
        return photocarnet;
    }

    public void setPhotocarnet(String photocarnet) {
        this.photocarnet = photocarnet;
    }

    public String getTpat() {
        return tpat;
    }

    public void setTpat(String tpat) {
        this.tpat = tpat;
    }

    public String getPnpat() {
        return pnpat;
    }

    public void setPnpat(String pnpat) {
        this.pnpat = pnpat;
    }

    public String getNpat() {
        return npat;
    }

    public void setNpat(String npat) {
        this.npat = npat;
    }
}
