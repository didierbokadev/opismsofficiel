package org.opisms.app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Cedric.BOKA on 13/02/2019.
 */


@Entity(
        tableName = "sms_user",
        indices = @Index(
                value = "sms_id",
                unique = true
        )
)
@SuppressWarnings("ALL")
public class SmsUser implements Parcelable {


    /**
     * ID_MSG : 5159095
     * MESSAGE : VOTRE ABONNEMENT AU CARNET ELECTRONIQUE DE VACCINATION EST RECONDUIT POUR 2 AN(S). ECHEANCE LE 05-02-2019. OPISMS VACCIN VOUS REMERCIE. 22521243489
     * DateMSG : 2017-02-05
     * HEUREMSG : 15:33:43
     * DATE_ENVOI : 2017-02-06 07:09:21
     */

    @Expose(deserialize = false, serialize = false)
    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    private long smsLocalId;
    @SerializedName("ID_MSG")
    @ColumnInfo(name = "sms_id")
    private String smsId;
    @SerializedName("MESSAGE")
    @ColumnInfo(name = "sms_body")
    private String smsBody;
    @SerializedName("DateMSG")
    @ColumnInfo(name = "sms_date")
    private String smsDate;
    @SerializedName("HEUREMSG")
    @ColumnInfo(name = "sms_heure")
    private String smsHeure;
    @SerializedName("DATE_ENVOI")
    @ColumnInfo(name = "sms_date_envoi")
    private String smsDateEnvoi;


    public SmsUser() {
    }

    public long getSmsLocalId() {
        return smsLocalId;
    }

    public void setSmsLocalId(long smsLocalId) {
        this.smsLocalId = smsLocalId;
    }

    public String getSmsId() {
        return smsId;
    }

    public void setSmsId(String smsId) {
        this.smsId = smsId;
    }

    public String getSmsBody() {
        return smsBody;
    }

    public void setSmsBody(String smsBody) {
        this.smsBody = smsBody;
    }

    public String getSmsDate() {
        return smsDate;
    }

    public void setSmsDate(String smsDate) {
        this.smsDate = smsDate;
    }

    public String getSmsHeure() {
        return smsHeure;
    }

    public void setSmsHeure(String smsHeure) {
        this.smsHeure = smsHeure;
    }

    public String getSmsDateEnvoi() {
        return smsDateEnvoi;
    }

    public void setSmsDateEnvoi(String smsDateEnvoi) {
        this.smsDateEnvoi = smsDateEnvoi;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.smsLocalId);
        dest.writeString(this.smsId);
        dest.writeString(this.smsBody);
        dest.writeString(this.smsDate);
        dest.writeString(this.smsHeure);
        dest.writeString(this.smsDateEnvoi);
    }

    protected SmsUser(Parcel in) {
        this.smsLocalId = in.readLong();
        this.smsId = in.readString();
        this.smsBody = in.readString();
        this.smsDate = in.readString();
        this.smsHeure = in.readString();
        this.smsDateEnvoi = in.readString();
    }

    public static final Parcelable.Creator<SmsUser> CREATOR = new Parcelable.Creator<SmsUser>() {
        @Override
        public SmsUser createFromParcel(Parcel source) {
            return new SmsUser(source);
        }

        @Override
        public SmsUser[] newArray(int size) {
            return new SmsUser[size];
        }
    };
}
