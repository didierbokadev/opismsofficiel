package org.opisms.app.model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("ALL")
@Entity(
        tableName = "vaccine",
        indices = {
                @Index(
                        value = "vacCalendrierId",
                        unique = true
                )
        }
)
public class Vaccine implements Parcelable {


    /**
     * type : 1
     * idcalendrier : 9561045
     * daterapel : 18-02-2019
     * datepresence : 18-02-2019
     * centre : HMA
     * vaccin : BCG
     * lot : 037G6247
     * imgcarnet :
     * flagvalidation : 0
     */

    @Expose(serialize = false, deserialize = false)
    @ColumnInfo(name = "_ID")
    @PrimaryKey(autoGenerate = true)
    private long localID;

    @SerializedName("type")
    private String vacType;
    @SerializedName("idcalendrier")
    private String vacCalendrierId;
    @SerializedName("daterapel")
    private String vacDateRappel;
    @SerializedName("datepresence")
    private String vacDatePresence;
    @SerializedName("centre")
    private String vacCenter;
    @SerializedName("vaccin")
    private String vacLabel;
    @SerializedName("lot")
    private String vacLot;
    @SerializedName("imgcarnet")
    private String vacImage;
    @SerializedName("flagvalidation")
    private String vacFalgValidation;


    public Vaccine() {
    }


    public long getLocalID() {
        return localID;
    }

    public void setLocalID(long localID) {
        this.localID = localID;
    }

    public String getVacType() {
        return vacType;
    }

    public void setVacType(String vacType) {
        this.vacType = vacType;
    }

    public String getVacCalendrierId() {
        return vacCalendrierId;
    }

    public void setVacCalendrierId(String vacCalendrierId) {
        this.vacCalendrierId = vacCalendrierId;
    }

    public String getVacDateRappel() {
        return vacDateRappel;
    }

    public void setVacDateRappel(String vacDateRappel) {
        this.vacDateRappel = vacDateRappel;
    }

    public String getVacDatePresence() {
        return vacDatePresence;
    }

    public void setVacDatePresence(String vacDatePresence) {
        this.vacDatePresence = vacDatePresence;
    }

    public String getVacCenter() {
        return vacCenter;
    }

    public void setVacCenter(String vacCenter) {
        this.vacCenter = vacCenter;
    }

    public String getVacLabel() {
        return vacLabel;
    }

    public void setVacLabel(String vacLabel) {
        this.vacLabel = vacLabel;
    }

    public String getVacLot() {
        return vacLot;
    }

    public void setVacLot(String vacLot) {
        this.vacLot = vacLot;
    }

    public String getVacImage() {
        return vacImage;
    }

    public void setVacImage(String vacImage) {
        this.vacImage = vacImage;
    }

    public String getVacFalgValidation() {
        return vacFalgValidation;
    }

    public void setVacFalgValidation(String vacFalgValidation) {
        this.vacFalgValidation = vacFalgValidation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.localID);
        dest.writeString(this.vacType);
        dest.writeString(this.vacCalendrierId);
        dest.writeString(this.vacDateRappel);
        dest.writeString(this.vacDatePresence);
        dest.writeString(this.vacCenter);
        dest.writeString(this.vacLabel);
        dest.writeString(this.vacLot);
        dest.writeString(this.vacImage);
        dest.writeString(this.vacFalgValidation);
    }

    protected Vaccine(Parcel in) {
        this.localID = in.readLong();
        this.vacType = in.readString();
        this.vacCalendrierId = in.readString();
        this.vacDateRappel = in.readString();
        this.vacDatePresence = in.readString();
        this.vacCenter = in.readString();
        this.vacLabel = in.readString();
        this.vacLot = in.readString();
        this.vacImage = in.readString();
        this.vacFalgValidation = in.readString();
    }

    public static final Creator<Vaccine> CREATOR = new Creator<Vaccine>() {
        @Override
        public Vaccine createFromParcel(Parcel source) {
            return new Vaccine(source);
        }

        @Override
        public Vaccine[] newArray(int size) {
            return new Vaccine[size];
        }
    };
}
