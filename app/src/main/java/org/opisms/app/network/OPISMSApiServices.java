package org.opisms.app.network;


import org.opisms.app.model.CommonResponse;
import org.opisms.app.model.Patient;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

@SuppressWarnings("ALL")
public interface OPISMSApiServices {


    @POST("user/register")
    Single<Patient> registerUser(@Body Patient patient);


    @FormUrlEncoded
    @POST("user/password/reset")
    Single<CommonResponse> resetPassword(@Field("email") String emailAdress);


    @Multipart
    @POST("upload")
    Completable uploadFile(@Part MultipartBody.Part file,
                           @Part("fileName") RequestBody fileName);
    
    
    
    @Multipart
    @POST("upload")
    Completable uploadFile(@Part MultipartBody.Part file,
                           @Part("fileName") RequestBody fileName,
                           @Part("userId") RequestBody userId);
}