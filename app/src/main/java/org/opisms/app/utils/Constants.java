package org.opisms.app.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;

/**
 * Created by Cedric.BOKA on 12/02/2019.
 */


@SuppressWarnings("All")
public class Constants {


    public static final String USER_PREFS = "user_preferences";
    public static final String USER_ID = "user_id";
    public static final String API_KEY = "60554287756c36b71b7e652.62297849";
    public static final String BUSINESS = "613434";


    public static boolean isValidDate(String input) {
        SimpleDateFormat formatDate = new SimpleDateFormat("ddMMyyyy");
        formatDate.setLenient(false);
        try {
            formatDate.parse(input);
            return true;
        }
        catch(ParseException e){
            return false;
        }
    }


    public static String generateTokenPayment() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);

        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }

        return buffer.toString();
    }


    public static String formatDate(String dateToFormat) {
        // Is return this format dd-MM-yyyy
        String date = "";
        date = String.valueOf(StringUtils.getDigits(dateToFormat));
        String day = StringUtils.substring(date, 0, 2);
        //LogUtils.e(day);
        String month = StringUtils.substring(date, 2, 4);
        //LogUtils.e(month);
        String year = StringUtils.substring(date, 4);
        //LogUtils.e(year);
        date = StringUtils.joinWith("-", day, month, year);
        return date;
    }
}
