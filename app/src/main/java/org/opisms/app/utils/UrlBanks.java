package org.opisms.app.utils;

/**
 * Created by Cedric.BOKA on 04/02/2019.
 */

@SuppressWarnings("ALL")
public class UrlBanks {

    //public static final String FLAVOR = "dev/";
    public static final String FLAVOR = "";

    public static final String LOGIN_USER = "https://www.opisms.net/opisms-ws/" + FLAVOR + "api/v1/user/login";
    public static final String REGISTER_USER = "https://www.opisms.net/opisms-ws/" + FLAVOR + "api/v1/user/register";
    public static final String LOAD_USER_CALENDRIERS = "https://www.opisms.net/opisms-ws/" + FLAVOR + "api/v1/user/calendriers";
    public static final String LOAD_USER_SMS = "https://www.opisms.net/opisms-ws/" + FLAVOR + "api/v1/user/sms";
    public static final String LOAD_USER_FAMILIES = "https://www.opisms.net/opisms-ws/" + FLAVOR + "api/v1/user/families";

    public static final String USER_VACCINE_INCOME_LIST = "https://www.opisms.net/myrecu-ws/prod/vaccin/rdv/prochain";
    public static final String USER_VACCINE_MISSED_LIST = "https://www.opisms.net/myrecu-ws/prod/vaccin/rdv/manque";
    public static final String USER_VACCINE_RIGHT_LIST = "https://www.opisms.net/myrecu-ws/prod/vaccin/rdv/ajour";
    public static final String USER_UPDATE_PASSWORD = "https://www.opisms.net/opisms-ws/" + FLAVOR + "api/v1/user/update/pass";
    public static final String USER_UPDATE_FORMULE = "https://www.opisms.net/opisms-ws/" + FLAVOR + "api/v1/user/formule/update";
    public static final String USER_SEND_MAIL = "https://www.opisms.net/opisms-ws/" + FLAVOR + "api/v1/user/send/mail";

}
