package org.opisms.app.worker;

import android.content.Context;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.SPUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.opisms.app.AppController;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.CalendrierVaccinal;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.UrlBanks;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.work.Worker;
import androidx.work.WorkerParameters;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Cedric.BOKA on 13/02/2019.
 */

@SuppressWarnings("ALL")
public class CalendrierWorker extends Worker {


    Gson gsonCalendriers;
    Type typeCalendriers;
    List<CalendrierVaccinal> calendrierVaccinals;
    private OpiSmsDatabase opiSmsDatabase;


    public CalendrierWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        gsonCalendriers = new GsonBuilder().serializeNulls().create();
        typeCalendriers = new TypeToken<ArrayList<CalendrierVaccinal>>(){}.getType();
        calendrierVaccinals = new ArrayList<>();
        opiSmsDatabase = OpiSmsDatabase.getDatabase(AppController.getContext());
    }


    @NonNull
    @Override
    public Result doWork() {
        if (!SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0").equals("0")) {
            try {
                OkHttpClient clientCalendriers = new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody paramsCalendriers = new FormBody.Builder()
                        .add("patientId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .build();

                Request requestCalendriers = new Request.Builder()
                        .url(UrlBanks.LOAD_USER_CALENDRIERS)
                        .post(paramsCalendriers)
                        .build();

                Response responseCalendriers = clientCalendriers.newCall(requestCalendriers).execute();
                String strCalendriers = responseCalendriers.body().string();

                if (responseCalendriers.isSuccessful()) {
                    JSONObject jsonCalendriers = new JSONObject(strCalendriers);

                    if (jsonCalendriers.getInt("code") == 0) {
                        calendrierVaccinals.clear();
                        calendrierVaccinals.addAll((ArrayList<CalendrierVaccinal>) gsonCalendriers.fromJson(jsonCalendriers.getJSONArray("data").toString(), typeCalendriers));

                        if (opiSmsDatabase.calendrierVaccinalDao().getCalendrierVaccinaux().size() != calendrierVaccinals.size()) {
                            for (CalendrierVaccinal calendrierVaccinal : calendrierVaccinals) {
                                opiSmsDatabase.calendrierVaccinalDao().createCalendrierVaccinal(calendrierVaccinal);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                // throw new RuntimeException(ex);
            }
        }

        return Result.success();
    }
}
