package org.opisms.app.worker;

import android.content.Context;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.SPUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.opisms.app.AppController;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.Family;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.UrlBanks;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.work.Worker;
import androidx.work.WorkerParameters;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Cedric.BOKA on 13/02/2019.
 */

@SuppressWarnings("ALL")
public class FamilyWorker extends Worker {


    Gson gsonFamilies;
    Type typeFamilies;
    List<Family> families;
    OpiSmsDatabase opiSmsDatabase;


    public FamilyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);

        gsonFamilies = new GsonBuilder().serializeNulls().create();
        typeFamilies = new TypeToken<ArrayList<Family>>(){}.getType();

        families = new ArrayList<>();
        opiSmsDatabase = OpiSmsDatabase.getDatabase(AppController.getContext());
    }


    @NonNull
    @Override
    public Result doWork() {

        if (!SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0").equals("0")) {
            try {
                OkHttpClient clientFamiles = new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody paramsFamiles = new FormBody.Builder()
                        .add("patientId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .build();

                Request requestFamiles = new Request.Builder()
                        .url(UrlBanks.LOAD_USER_FAMILIES)
                        .post(paramsFamiles)
                        .build();

                Response responseFamiles = clientFamiles.newCall(requestFamiles).execute();
                String strFamiles = responseFamiles.body().string();

                if (responseFamiles.isSuccessful()) {
                    JSONObject jsonFamiles = new JSONObject(strFamiles);

                    if (jsonFamiles.getInt("code") == 0) {
                        families.clear();
                        families.addAll((ArrayList<Family>) gsonFamilies.fromJson(jsonFamiles.getJSONArray("data").toString(), typeFamilies));

                        if (opiSmsDatabase.familyDao().getFamilies().size() != families.size()) {
                            opiSmsDatabase.familyDao().deleteAll();

                            for (Family family : families) {
                                opiSmsDatabase.familyDao().createFamily(family);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                //throw new RuntimeException(ex);
            }
        }

        return Result.success();
    }
}
