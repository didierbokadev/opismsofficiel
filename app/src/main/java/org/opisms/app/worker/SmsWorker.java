package org.opisms.app.worker;

import android.content.Context;
import androidx.annotation.NonNull;

import com.blankj.utilcode.util.SPUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.opisms.app.AppController;
import org.opisms.app.datas.database.OpiSmsDatabase;
import org.opisms.app.model.SmsUser;
import org.opisms.app.utils.Constants;
import org.opisms.app.utils.UrlBanks;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.work.Worker;
import androidx.work.WorkerParameters;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Cedric.BOKA on 13/02/2019.
 */

@SuppressWarnings("ALL")
public class SmsWorker extends Worker {


    Gson gsonSms;
    Type typeSms;
    List<SmsUser> smsUsers;
    OpiSmsDatabase opiSmsDatabase;


    public SmsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);

        gsonSms = new GsonBuilder().serializeNulls().create();
        typeSms = new TypeToken<ArrayList<SmsUser>>(){}.getType();

        smsUsers = new ArrayList<>();

        opiSmsDatabase = OpiSmsDatabase.getDatabase(AppController.getContext());
    }


    @NonNull
    @Override
    public Result doWork() {

        if (!SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0").equals("0")) {
            try {
                OkHttpClient clientSms = new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                RequestBody paramsSms= new FormBody.Builder()
                        .add("patientId", SPUtils.getInstance(Constants.USER_PREFS).getString(Constants.USER_ID, "0"))
                        .build();

                Request requestSms = new Request.Builder()
                        .url(UrlBanks.LOAD_USER_SMS)
                        .post(paramsSms)
                        .build();

                Response responseSms = clientSms.newCall(requestSms).execute();
                String strSms = responseSms.body().string();

                if (responseSms.isSuccessful()) {
                    JSONObject jsonSms = new JSONObject(strSms);

                    if (jsonSms.getInt("code") == 0) {
                        smsUsers.clear();
                        smsUsers.addAll((ArrayList<SmsUser>) gsonSms.fromJson(jsonSms.getJSONArray("data").toString(), typeSms));

                        if (opiSmsDatabase.smsUserDao().getSms().size() == 0 || opiSmsDatabase.smsUserDao().getSms().size() != smsUsers.size()) {
                            opiSmsDatabase.smsUserDao().deleteAll();

                            for (SmsUser smsUser : smsUsers) {
                                opiSmsDatabase.smsUserDao().createUserSms(smsUser);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                // throw new RuntimeException(ex);
                //code = 4;
            }
        }

        return Result.success();
    }
}
